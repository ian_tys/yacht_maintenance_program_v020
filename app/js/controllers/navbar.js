// Used to toggle the menu on small screens when clicking on the menu button
function showthelist() {
    var x = document.getElementById("navDlist");
    if (x.className.indexOf("ymp-show") == -1) {
        x.className += " ymp-show";
        hidetheprofile();
    } else {
        hidethelist();
    }
}

function showtheprofile() {
    var x = document.getElementById("navDprofile");
    if (x.className.indexOf("ymp-show") == -1) {
        x.className += " ymp-show";
        hidethelist();
    } else {
        hidetheprofile();
    }
}

function hidethelist() {
  var x = document.getElementById("navDlist");
  x.className = x.className.replace(" ymp-show", "");
}

function hidetheprofile() {
  var x = document.getElementById("navDprofile");
  x.className = x.className.replace(" ymp-show", "");
}

// Used to go to pages
function get_page(page_url) {
  if (page_url == "login.htm")  {
    var get_body = document.getElementsByTagName("BODY")[0];
    var get_head = document.getElementById("login_head");
    var get_profilebar = document.getElementById("ympnavbar_itemp");

    get_body.style.overflow = "hidden";
    get_head.setAttribute("style", "display: inline;");
    get_profilebar.setAttribute("style", "display: none;");

    document.getElementById('ymp_page').src = "./pages/" + page_url;
  }
  else {
    document.getElementById('ymp_page').src = "./pages/" + page_url;
  }
}
