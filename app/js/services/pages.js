// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {

  // get the modal
  var modal = document.getElementById('modal_edit');

  if (event.target == modal) {
    try{win.close();}catch(err){}
    modal.style.display = "none";
    location.reload();
  }
}

// Buttons

// Enable edit records
function editoptions() {
  var get_data = document.getElementById('number_of_records').innerHTML;
  var record_count = get_data.substring(get_data.indexOf(":")+1);

  // enable all options edit
  for (var i = 0; i < record_count; i++) {
    if (document.getElementById("detailsbtn" + i).style.display == 'block'){
      document.getElementById("detailsbtn" + i).style.display='none';
    }
    else {
      document.getElementById("detailsbtn" + i).style.display='block';
    }
  }
}
// /editoptions

// /Buttons

// When the user clicks for record details
function get_details(json_fields, json_file, file_id)
{

  if (file_id.slice(-2) == "ua") {
    document.location.replace("profile.htm?var=" + file_id);
  }
  else {

    if ((file_id.slice(-2) == "lb") || (file_id.slice(-2) == "jm") || (file_id.slice(-2) == "im")) {

      refreshform(json_fields, json_file, file_id);

      document.getElementById("modal_edit").style.display="block";

      document.getElementById("addlblmodal").style.display="inline";
      document.getElementById("oklblmodal").style.display="none";
      document.getElementById("addbtnmodal").style.display="none";
      document.getElementById("okbtnmodal").style.display="none";
      document.getElementById("editbtnmodal").style.display="inline";
      document.getElementById("updatebtnmodal").style.display="none";
      document.getElementById("deletebtnmodal").style.display="inline";
      document.getElementById("confirmdeletebtnmodal").style.display="none";
      document.getElementById("rcancelbtnmodal").style.display="none";
      document.getElementById("gcancelbtnmodal").style.display="inline";
    }
    else {

      if (json_fields == "") {

        var script_calledittable = document.createElement("script");
        script_calledittable.innerHTML = "detailsfield('" + json_file + "','" + file_id  + "');";
        document.body.appendChild(script_calledittable);

        document.getElementById('modal_edit').style.display="block";

        // add the buttons
        var btnContainer = document.getElementById("btnboardmodal");
        btnContainer.innerHTML = "";

        var btn_edit = document.createElement("BUTTON");
        var btn_edit_text = document.createTextNode("Edit");
        btn_edit.setAttribute("type", "button");
        btn_edit.setAttribute("class", "greenbtnmodal");
        btn_edit.setAttribute("onclick", "editfield('" + json_file + "','" + file_id  + "');");
        btn_edit.appendChild(btn_edit_text);

        var btn_delete = document.createElement("BUTTON");
        var btn_delete_text = document.createTextNode("Delete");
        btn_delete.setAttribute("type", "button");
        btn_delete.setAttribute("class", "redbtnmodal");
        btn_delete.setAttribute("onclick", "deletefield('" + json_file + "','" + file_id  + "');");
        btn_delete.appendChild(btn_delete_text);

        var btn_cancel = document.createElement("BUTTON");
        var btn_cancel_text = document.createTextNode("Cancel");
        btn_cancel.setAttribute("type", "button");
        btn_cancel.setAttribute("class", "greenbtnmodal");
        btn_cancel.setAttribute("onclick", "document.getElementById('modal_edit').style.display='none';");
        btn_cancel.appendChild(btn_cancel_text);

        btnContainer.appendChild(btn_edit);
        btnContainer.appendChild(btn_delete);
        btnContainer.appendChild(btn_cancel);

      }

      // used for all the picklists getdetails in the settings
      if (json_fields == "picklist") {

        var script_calledittable = document.createElement("script");
        script_calledittable.innerHTML = "setfields('" + json_file + "');";
        document.body.appendChild(script_calledittable);

      }
      else {
        // used for all the picklists getdetails in the main pages
        if (json_fields == "main_picklist") {

          if (json_file.search("job.json") != -1) {
            top.document.getElementById('ymp_page').src = "./pages/job.htm";
          }
          else {
            if (json_file.search("inventory.json") != -1) {
              top.document.getElementById('ymp_page').src = "./pages/inventory.htm";
            }
            else {
              if (json_file.search("logbook.json") != -1) {
                top.document.getElementById('ymp_page').src = "./pages/logbook.htm";
              }
              else {
                if (json_file.search("user.json") != -1) {
                  top.document.getElementById('ymp_page').src = "./pages/employee.htm";
                }
                else {
                  if (json_file.search("watchkeeper.json") != -1) {
                    top.document.getElementById('ymp_page').src = "./pages/watchkeeper.htm";
                  }
                  else {
                    //top.document.getElementById('ymp_page').src = "./pages/settings.htm?var=" +
                    top.document.getElementById('ymp_page').src = "./pages/settings.htm?=&" +
                      json_file + "&" + file_id;
                  }
                }
              }
            }
          }

        }
      }

    }

  }
}
// /get_details
