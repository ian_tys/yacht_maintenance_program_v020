// Used to authenticate and login
function getsecure() {
  var sign_username = document.getElementById("inputusername").value;
  var sign_password = document.getElementById("inputpassword").value;

  if (sign_username != "" && sign_password != "") {

    var JSONfile = top.getJSONfile("", "tys_production/ympdb/user.json");

    // get all users in JSON file - 3rd array in getJSONfile
    var content_records = JSONfile[2];

    if (content_records != "") {

      var auth_user = top.getObjects(content_records, 'user_username', sign_username);

      if (auth_user != '') {
        if (auth_user[0].user_username == sign_username
          && auth_user[0].user_password == sign_password) {

            var get_head = top.document.getElementById("login_head");
            var get_profilebar = top.document.getElementById("ympnavbar_itemp");

            get_head.setAttribute("style", "display: none;");
            get_profilebar.setAttribute("style", "display: inline-block;");

            var get_profilelink = top.document.getElementById("userprofilelink");
            var get_profilepage = top.document.getElementById("userprofilebtn");

            top.document.getElementById("user_username_content").innerHTML = auth_user[0].user_username;
            top.document.getElementById("user_username_content").title = auth_user[0].user_id;

            JSONfile = top.getJSONfile("", "tys_production/ympdb/picklist/user_title.json");
            var picklist_records = JSONfile[2];
            var auth_title = top.getObjects(picklist_records, 'user_title_id', auth_user[0].user_title);
            top.document.getElementById("user_title_content").innerHTML = auth_title[0]["user_title"];

            if (auth_user[0].user_pic != "") {
              top.document.getElementById("user_pic_content").src =
                location.href.slice(8, 11) + auth_user[0].user_pic;
            }

            get_profilepage.setAttribute("onclick",
              "get_page('profile.htm?var=" + auth_user[0].user_id + "'); hidetheprofile();");
            get_profilelink.appendChild(get_profilepage);

            // window.location.assign("dashboard.htm");
            window.location.assign("job.htm");
        }
        else {
          alert('Incorrect credentials! Please check!');
        }
      }
      else {
        alert('Incorrect credentials! Please check!');
      }
    }
    else {
      alert('There are no users in the current database!');
    }
  }
  else {

  }
}
