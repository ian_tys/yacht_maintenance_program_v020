  function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {

      // Only process image files.
      if (!f.type.match('image.*')) {
        continue;
      }

      var reader = new FileReader();

      // Closure to capture the file information.
      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('list').insertBefore(span, null);
        };
      })(f);

      // Read in the image file as a data URL.
      reader.readAsDataURL(f);
    }
  }

  document.addEventListener('DOMContentLoaded', function () {
  try{document.getElementById('files').addEventListener('change', handleFileSelect, false);}catch(err){}
});


function submitAttach() {

  var input = document.getElementById('file');
  var file = input.files[0]; // file is a Blob

  var fs = top.require('fs');
  var buffer = top.require('buffer').Buffer;

  var fileBuffer;
}

function onChange(event) {

  var input = document.getElementById('file');

  var file = input.files[0];

  var reader = new FileReader();
   reader.onload = function(event) {
     fs.writeFile('logo.png', event.target.result, function(err){
                if (err) throw err
                console.log('File saved.')
            })
   };

  reader.readAsArrayBuffer(file);

}


//doStuff(file);
  //console.log(fileBuffer);
