// To service all the connections to JSON

// To retrieve all data related to JSON File
function getJSONfile(json_fields, json_file) {

  // Get the File System
  var fs = top.require('fs');

  var content_fields = [];
  var content_fields_head = [];
  var content_records = [];
  var content_records_head = [];

  if (json_fields != "") {
    var json_contents = fs.readFileSync(location.href.slice(8, 11) + json_fields);
    content_fields = JSON.parse(json_contents);

    for (var i = 0; i < content_fields.length; i++) {
      for (var key in content_fields[i]) {
        if (content_fields_head.indexOf(key) === -1) {
          content_fields_head.push(key);
        }
      }
    }
  }

  //console.log(json_file);
  if (json_file != "") {
    var json_contents = fs.readFileSync(location.href.slice(8, 11) + json_file);

    try {
      content_records = JSON.parse(json_contents);

      for (var i = 0; i < content_records.length; i++) {
        for (var key in content_records[i]) {
          if (content_records_head.indexOf(key) === -1) {
            content_records_head.push(key);
          }
        }
      }
    }
    catch (err) {}

  }

  return [content_fields, content_fields_head, content_records, content_records_head];
}
// /getJSONfile

// Add record to JSON file
function addJSONrecord(json_file, new_data) {

  // Get the File System
  var fs = top.require('fs');

  var JSONfile = getJSONfile("", json_file);

  var content_records = JSONfile[2];

  // add new record
  content_records.push(new_data);

  fs.writeFile(
    location.href.slice(8, 11) + json_file,
    JSON.stringify(content_records),
    'utf-8',
    function(err)
  {
    if (err) alert(err);
  });

  return null;
}
// /addJSONrecord

// Refresh JSON file by deleting the old content first
function refreshJSONrecord(json_file, new_data) {

  // Get the File System
  var fs = top.require('fs');

  var JSONfile = getJSONfile("", json_file);

  // empty JSON file from previous content
  var content_records = [];

  if (content_records[0] != "[]") {
    // add new record
    content_records.push(new_data);
  }

  fs.writeFile(
    location.href.slice(8, 11) + json_file,
    JSON.stringify(content_records),
    'utf-8',
    function(err)
  {
    if (err) alert(err);
  });

  return null;
}
// /addJSONrecord

// Update JSON file
function updateJSONfile(json_file, file_id, new_data) {

  // Get the File System
  var fs = top.require('fs');

  var JSONfile = getJSONfile("", json_file);

  var content_records = JSONfile[2];
  var content_records_head = JSONfile[3];

  var json_index = 0

  for (var i=0; i < content_records.length; i++) {
    if (content_records[i][content_records_head[0]] == file_id) {
      json_index = i;
    }
  }

  // replace old record with new
  content_records.splice(json_index, 1, new_data);

  fs.writeFile(
    location.href.slice(8, 11) + json_file,
    JSON.stringify(content_records),
    'utf-8',
    function(err)
  {
    if (err) alert(err);
  });

  return null;
}
// /updateJSONfile

// Delete record from JSON file
function deleteJSONrecord(json_file, file_id) {

  // Get the File System
  var fs = top.require('fs');

  var JSONfile = getJSONfile("", json_file);

  var content_records = JSONfile[2];
  var content_records_head = JSONfile[3];

  var json_index = 0

  for (var i=0; i < content_records.length; i++) {
    if (content_records[i][content_records_head[0]] == file_id) {
      json_index = i;
    }
  }

  // delete record
  content_records.splice(json_index, 1);

  fs.writeFile(
    location.href.slice(8, 11) + json_file,
    JSON.stringify(content_records),
    'utf-8',
    function(err)
  {
    if (err) alert(err);
  });

  return null;
}
// /deleteJSONrecord

// Find the right record

//return an array of objects according to key, value, or key and value matching
function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else
        //if key matches and value matches or if key matches and value is not passed (eliminating the case where key matches but passed value does not)
        if (i == key && obj[i] == val || i == key && val == '') { //
            objects.push(obj);
        } else if (obj[i] == val && key == ''){
            //only add if the object is not already in the array
            if (objects.lastIndexOf(obj) == -1){
                objects.push(obj);
            }
        }
    }
    return objects;
}
// /getObjects
