// declare variables
const {app, BrowserWindow} = require("electron");
const path = require("path");
const url = require("url");

// init win
let win;

function createWindow(){

  // Create browser window
  win = new BrowserWindow({
    width:1185,
    height:800,

    minWidth: 580,
    minHeight: 720,

    icon:__dirname+"/img/ymp_icon_v_0_5.png",
    //frame: false,
    show:false
  });

  // remove default menu
  win.setMenu(null);

  // Load index.html
  win.loadURL(url.format({
    //pathname: path.join(__dirname, '/pages/login.htm'),
    pathname: path.join(__dirname, "index.html"),
    protocol: "file:",
    slashes: true
  }));

  // Open devtools
  win.webContents.openDevTools();

  // show BrowserWindow when Chromium loads completely
    // other then .on there are: .show; .hide; .focus; .close
  win.on("ready-to-show", () => {
    win.show()
  });

  win.on("closed", () => {
    app.quit();
    win = null;
  });
}

// Run create window function
app.on("ready", createWindow);

// Quit when all windows are closed
app.on("window-all-closed", () => {
  if(process.platform !== "darwin"){
    app.quit();
  }
});
