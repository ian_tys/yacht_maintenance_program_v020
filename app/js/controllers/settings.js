// Get the page url
var url_data = window.location.toString();
// IF a variable is not passed then
if (url_data.search("=") == -1) {
  var file_pick = "";
}
// Else if a variable is passed then
else {
  //var file_pick = url_data.substring(url_data.indexOf("=")+1);
  var parameters = location.search.substring(1).split("&");
  var file_pick = parameters[1];
  var file_exception = parameters[2];
}

// Load setting according to passed variable
function refreshsettings() {
  if (file_pick != "") {
    fields_action(file_pick);
  }
}
// /refreshsettings

// Initiate fields editing
function setfields(json_file) {
  document.getElementById("modal_edit").style.display="none";

  document.getElementById("options_title").innerHTML =
    "Set "
    +
    json_file.substr(json_file.lastIndexOf('/') + 1, json_file.length - json_file.lastIndexOf('/') - 6)
    +
    " :";

  if (file_pick == "") {
    var btn_back = document.getElementById('modalbtn_back')
    btn_back.setAttribute("onclick", "location.reload();");
    btn_back.setAttribute("style", "display: inline;");
  }
  else {
    // throws back page which called settings
    if (file_pick.search("job") != -1) {
      var btn_back = document.getElementById('modalbtn_back')
      btn_back.setAttribute("onclick",
        "top.document.getElementById('ymp_page').src = './pages/job.htm';");
      btn_back.setAttribute("style", "display: inline;");
    }
    else {
      if (file_pick.search("inventory") != -1) {
        var btn_back = document.getElementById('modalbtn_back')
        btn_back.setAttribute("onclick",
          "top.document.getElementById('ymp_page').src = './pages/inventory.htm';");
        btn_back.setAttribute("style", "display: inline;");
      }
      else {
        if (file_pick.search("logbook") != -1) {
          var btn_back = document.getElementById('modalbtn_back')
          btn_back.setAttribute("onclick",
            "top.document.getElementById('ymp_page').src = './pages/logbook.htm';");
          btn_back.setAttribute("style", "display: inline;");
        }
        else {
          if (file_pick.search("user") != -1) {

            var btn_back = document.getElementById('modalbtn_back')
            if (file_exception == "job_exception") {
              btn_back.setAttribute("onclick",
              "top.document.getElementById('ymp_page').src = './pages/job.htm';");
            }
            else {
              btn_back.setAttribute("onclick",
              "top.document.getElementById('ymp_page').src = './pages/employee.htm';");
            }

            btn_back.setAttribute("style", "display: inline;");
          }
          else {
            if (file_pick.search("watchkeeper") != -1) {
              var btn_back = document.getElementById('modalbtn_back')
              btn_back.setAttribute("onclick",
                "top.document.getElementById('ymp_page').src = './pages/watchkeeper.htm';");
              btn_back.setAttribute("style", "display: inline;");
            }
          }
        }
      }
    }
  }

  document.getElementById("addlblmodal").style.display = "inline";
  document.getElementById("oklblmodal").style.display = "none";
  document.getElementById("deletelblmodal").style.display = "none";

  var script_callrefreshtable = document.createElement("script");
  script_callrefreshtable.innerHTML =
    // "try{refreshtable('', '" + json_file + "');}catch(err){alert('Non-existant picklist selected!');}";
    "refreshtable('', '" + json_file + "');";
  document.body.appendChild(script_callrefreshtable);

  // add the buttons
  var fields_buttons = document.getElementById("btnboardmain");
  fields_buttons.innerHTML = "";

  var btn_add = document.createElement("button");
  var btn_add_text = document.createTextNode("Add");
  btn_add.setAttribute("class", "addbtn");
  btn_add.setAttribute("onclick",
    "document.getElementById('modal_edit').style.display='block'; addfield('" + json_file + "');");
  btn_add.appendChild(btn_add_text);

  var btn_edit = document.createElement("button");
  var btn_edit_text = document.createTextNode("Edit");
  btn_edit.setAttribute("class", "editbtn");
  btn_edit.setAttribute("onclick", "editoptions();");
  btn_edit.appendChild(btn_edit_text);

  fields_buttons.appendChild(btn_add);
  fields_buttons.appendChild(btn_edit);
}
// /setfields

// Setting Buttons

// Edit the fields of the table called
function fields_action(json_file) {
  setfields(json_file);

  document.getElementById("div_tables").style = "float: none;";
  document.getElementById("table_settings").style = "visibility: hidden; position: absolute;";
  document.getElementById("table_picklists").style = "visibility: hidden; position: absolute;";
}

// /Setting Buttons

// Create a new dynamic form to add new field
function addfield(json_file) {
  JSONfile = top.getJSONfile("", json_file);

  content_records = JSONfile[2];
  content_records_head = JSONfile[3];

  // create dynamic table
  var ymp_table = document.createElement("table");

  // add JSON data to the table as rows
  for (var i = 0; i < content_records_head.length; i++) {
    var ymp_tr = ymp_table.insertRow(-1);             // table row

    var ymp_td = ymp_tr.insertCell(-1);              // table cell1
    // show record field title
    ymp_td.innerHTML = content_records_head[i];

    var ymp_td = ymp_tr.insertCell(-1);              // table cell2

    // insert drop downs according to field
    if (content_records_head[i] == "field_type") {
      var tabinput = document.createElement("select");
      tabinput.setAttribute("id", content_records_head[i]);

      var type1 = document.createElement("option");
      type1.setAttribute("value", "text");
      var type1text = document.createTextNode("text");
      type1.appendChild(type1text);

      var type2 = document.createElement("option");
      type2.setAttribute("value", "number");
      var type2text = document.createTextNode("number");
      type2.appendChild(type2text);

      var type3 = document.createElement("option");
      type3.setAttribute("value", "boolean");
      var type3text = document.createTextNode("boolean");
      type3.appendChild(type3text);

      var type4 = document.createElement("option");
      type4.setAttribute("value", "password");
      var type4text = document.createTextNode("password");
      type4.appendChild(type4text);

      var type5 = document.createElement("option");
      type5.setAttribute("value", "date");
      var type5text = document.createTextNode("date");
      type5.appendChild(type5text);

      var type6 = document.createElement("option");
      type6.setAttribute("value", "datetime-local");
      var type6text = document.createTextNode("datetime-local");
      type6.appendChild(type6text);

      tabinput.appendChild(type1);
      tabinput.appendChild(type2);
      tabinput.appendChild(type3);
      tabinput.appendChild(type4);
      tabinput.appendChild(type5);
      tabinput.appendChild(type6);
    }
    else {
      if (content_records_head[i] == "field_group") {
        var tabinput = document.createElement("select");
        tabinput.setAttribute("id", content_records_head[i]);

        var group1 = document.createElement("option");
        group1.setAttribute("value", "_lb");
        var group1text = document.createTextNode("_lb");
        group1.appendChild(group1text);

        var group2 = document.createElement("option");
        group2.setAttribute("value", "_jm");
        var group2text = document.createTextNode("_jm");
        group2.appendChild(group2text);

        var group3 = document.createElement("option");
        group3.setAttribute("value", "_im");
        var group3text = document.createTextNode("_im");
        group3.appendChild(group3text);

        var group4 = document.createElement("option");
        group4.setAttribute("value", "_ua");
        var group4text = document.createTextNode("_ua");
        group4.appendChild(group4text);

        var group5 = document.createElement("option");
        group5.setAttribute("value", "_pk");
        var group5text = document.createTextNode("_pk");
        group5.appendChild(group5text);

        tabinput.appendChild(group1);
        tabinput.appendChild(group2);
        tabinput.appendChild(group3);
        tabinput.appendChild(group4);
        tabinput.appendChild(group5);
      }
      else {
        if (content_records_head[i] == "field_view") {
          var tabinput = document.createElement("select");
          tabinput.setAttribute("id", content_records_head[i]);

          var view1 = document.createElement("option");
          view1.setAttribute("value", "main");
          var view1text = document.createTextNode("main");
          view1.appendChild(view1text);

          var view2 = document.createElement("option");
          view2.setAttribute("value", "details");
          var view2text = document.createTextNode("details");
          view2.appendChild(view2text);

          var view3 = document.createElement("option");
          view3.setAttribute("value", "pii");
          var view3text = document.createTextNode("pii");
          view3.appendChild(view3text);

          var view4 = document.createElement("option");
          view4.setAttribute("value", "log");
          var view4text = document.createTextNode("log");
          view4.appendChild(view4text);

          tabinput.appendChild(view1);
          tabinput.appendChild(view2);
          tabinput.appendChild(view3);
          tabinput.appendChild(view4);
        }
        else {
          if (content_records_head[i] == "field_input") {
            var tabinput = document.createElement("select");
            tabinput.setAttribute("id", content_records_head[i]);

            var input1 = document.createElement("option");
            input1.setAttribute("value", "auto");
            var input1text = document.createTextNode("auto");
            input1.appendChild(input1text);

            var input2 = document.createElement("option");
            input2.setAttribute("value", "write");
            var input2text = document.createTextNode("write");
            input2.appendChild(input2text);

            var input3 = document.createElement("option");
            input3.setAttribute("value", "picklist");
            var input3text = document.createTextNode("picklist");
            input3.appendChild(input3text);

            var input4 = document.createElement("option");
            input4.setAttribute("value", "file");
            var input4text = document.createTextNode("file");
            input4.appendChild(input4text);

            tabinput.appendChild(input1);
            tabinput.appendChild(input2);
            tabinput.appendChild(input3);
            tabinput.appendChild(input4);
          }
          else {
            if (
              (content_records_head[i] == "field_disabled")
              ||
              (content_records_head[i] == "field_static")
              ||
              (content_records_head[i] == "field_required")
              ||
              (content_records_head[i] == "watch_main")
              ) {
                var tabinput = document.createElement("select");
                tabinput.setAttribute("id", content_records_head[i]);

                var input1 = document.createElement("option");
                input1.setAttribute("value", true);
                var input1text = document.createTextNode(true);
                input1.appendChild(input1text);

                var input2 = document.createElement("option");
                input2.setAttribute("value", false);
                var input2text = document.createTextNode(false);
                input2.appendChild(input2text);

                tabinput.appendChild(input1);
                tabinput.appendChild(input2);
            }
            else {
              // input box
              var tabinput = document.createElement("input");
              if (i == 0) {
                tabinput.setAttribute("type", "number");
                tabinput.setAttribute("value", content_records.length + 1);
              }
              else {
                tabinput.setAttribute("type", "text");
              }
              if (content_records_head[i] == "field_picklist") {
                tabinput.setAttribute("placeholder", "Enter picklist name");
              }
              tabinput.setAttribute("id", content_records_head[i]);
              if (i == 0) {
                tabinput.setAttribute("disabled", "disabled");
              }
            }
          }
        }
      }
    }

    ymp_td.appendChild(tabinput);
  }

  // finally add the newly created table with JSON data to a container
  var divContainer = document.getElementById("show_records_input1");
  divContainer.innerHTML = "";
  divContainer.appendChild(ymp_table);

  // add the buttons
  var btnContainer = document.getElementById("btnboardmodal");
  btnContainer.innerHTML = "";

  var btn_add = document.createElement("button");
  var btn_add_text = document.createTextNode("Add");
  btn_add.setAttribute("type", "button");
  btn_add.setAttribute("class", "greenbtnmodal");
  btn_add.setAttribute("onclick", "addconfirmfield('" + json_file + "');");
  btn_add.appendChild(btn_add_text);

  var btn_cancel = document.createElement("button");
  var btn_cancel_text = document.createTextNode("Cancel");
  btn_cancel.setAttribute("type", "button");
  btn_cancel.setAttribute("class", "redbtnmodal");
  btn_cancel.setAttribute("onclick", "document.getElementById('modal_edit').style.display='none';");
  btn_cancel.appendChild(btn_cancel_text);

  btnContainer.appendChild(btn_add);
  btnContainer.appendChild(btn_cancel);
}
// /addfield

// Confirm the addition of a new field
function addconfirmfield (json_file) {

  var JSONfile = top.getJSONfile("", json_file);

  var content_records = JSONfile[2];
  var content_records_head = JSONfile[3];

  // get the content to add

  // if header belongs of fields or picklist then
  if (
    (json_file.search("field") != -1)
    ||
    (json_file.search("picklist") != -1)
    ) {
      var new_records = '"' + content_records_head[0] + '"' + ':' +
      document.getElementById(content_records_head[0]).value;  }
  else {
    var new_records = '"' + content_records_head[0] + '"' + ':' +
    '"' + document.getElementById(content_records_head[0]).value + '"';
  }
  // go through all the data
  for (var j = 1; j < content_records_head.length; j++) {
    // specify the fields that you want to get no string value stored
    if (
      (content_records_head[j] == "field_disabled")
      ||
      (content_records_head[j] == "field_static")
      ||
      (content_records_head[j] == "field_required")
    ) {
      new_records = new_records + ", " + '"' + content_records_head[j] + '"' + ":" +
      document.getElementById(content_records_head[j]).value;
    }
    else {
      new_records = new_records + ", " + '"' + content_records_head[j] + '"' + ":" +
      '"' + document.getElementById(content_records_head[j]).value + '"';
    }
  }
  new_records = "{" + new_records + "}";
  new_records = JSON.parse(new_records);

  // disable all inputs
  for (var i = 0; i < content_records_head.length; i++) {
    document.getElementById(content_records_head[i]).disabled = "disabled";
  }

  document.getElementById("addlblmodal").style.display = "none";
  document.getElementById("oklblmodal").style.display = "inline";

  // add the buttons
  var btnContainer = document.getElementById('btnboardmodal');
  btnContainer.innerHTML = "";

  var btn_ok = document.createElement("button");
  var btn_ok_text = document.createTextNode("Ok");
  btn_ok.setAttribute("type", "button");
  btn_ok.setAttribute("class", "greenbtnmodal");
  btn_ok.setAttribute("onclick", "okfield('" + json_file + "');");
  btn_ok.appendChild(btn_ok_text);

  btnContainer.appendChild(btn_ok);

  top.addJSONrecord(json_file, new_records);
}
// /addconfirmfield

// Create a new dynamic form to display all details
function detailsfield(json_file, file_id) {
  var JSONfile = top.getJSONfile("", json_file);

  var content_records = JSONfile[2];
  var content_records_head = JSONfile[3];

  // get the field related records to edit
  var fieldtoedit = top.getObjects(content_records, content_records_head[0], file_id)

  // create dynamic table
  var ymp_table = document.createElement("table");

  // add JSON data to the table as rows
  for (var i = 0; i < content_records_head.length; i++) {

    var ymp_tr = ymp_table.insertRow(-1);             // table row

    var ymp_td = ymp_tr.insertCell(-1);              // table cell1
    // show record field title
    ymp_td.innerHTML = content_records_head[i];

    var ymp_td = ymp_tr.insertCell(-1);              // table cell2

    // input box
    var tabinput = document.createElement("input");
    tabinput.setAttribute("id", content_records_head[i]);
    tabinput.setAttribute("type", "text");
    tabinput.setAttribute("value", fieldtoedit[0][content_records_head[i]]);
    tabinput.setAttribute("disabled", "disabled");

    ymp_td.appendChild(tabinput);
  }

  // finally add the newly created table with JSON data to a container
  var divContainer = document.getElementById("show_records_input1");
  divContainer.innerHTML = "";
  divContainer.appendChild(ymp_table);
}
// /detailsfield

// Create a new dynamic form to edit a field
function editfield(json_file, file_id) {
  var JSONfile = top.getJSONfile("", json_file);

  var content_records = JSONfile[2];
  var content_records_head = JSONfile[3];

  // get the field related records to edit
  var fieldtoedit = top.getObjects(content_records, content_records_head[0], file_id)

  // create dynamic table
  var ymp_table = document.createElement("table");

  // add JSON data to the table as rows
  for (var i = 0; i < content_records_head.length; i++) {

    var ymp_tr = ymp_table.insertRow(-1);             // table row

    var ymp_td = ymp_tr.insertCell(-1);              // table cell1
    // show record field title
    ymp_td.innerHTML = [content_records_head[i]];

    var ymp_td = ymp_tr.insertCell(-1);              // table cell2

    // insert drop downs according to field
    if (content_records_head[i] == "field_type") {
      var tabinput = document.createElement("select");
      tabinput.setAttribute("id", content_records_head[i]);

      var type1 = document.createElement("option");
      type1.setAttribute("value", "text");
      if (fieldtoedit[0][content_records_head[i]] == "text") {
        type1.setAttribute("selected", "true");
      }
      var type1text = document.createTextNode("text");
      type1.appendChild(type1text);

      var type2 = document.createElement("option");
      type2.setAttribute("value", "number");
      if (fieldtoedit[0][content_records_head[i]] == "number") {
        type2.setAttribute("selected", "true");
      }
      var type2text = document.createTextNode("number");
      type2.appendChild(type2text);

      var type3 = document.createElement("option");
      type3.setAttribute("value", "boolean");
      if (fieldtoedit[0][content_records_head[i]] == "boolean") {
        type3.setAttribute("selected", "true");
      }
      var type3text = document.createTextNode("boolean");
      type3.appendChild(type3text);

      var type4 = document.createElement("option");
      type4.setAttribute("value", "password");
      if (fieldtoedit[0][content_records_head[i]] == "password") {
        type4.setAttribute("selected", "true");
      }
      var type4text = document.createTextNode("password");
      type4.appendChild(type4text);

      var type5 = document.createElement("option");
      type5.setAttribute("value", "date");
      if (fieldtoedit[0][content_records_head[i]] == "date") {
        type5.setAttribute("selected", "true");
      }
      var type5text = document.createTextNode("date");
      type5.appendChild(type5text);

      var type6 = document.createElement("option");
      type6.setAttribute("value", "datetime-local");
      if (fieldtoedit[0][content_records_head[i]] == "datetime-local") {
        type6.setAttribute("selected", "true");
      }
      var type6text = document.createTextNode("datetime-local");
      type6.appendChild(type6text);

      tabinput.appendChild(type1);
      tabinput.appendChild(type2);
      tabinput.appendChild(type3);
      tabinput.appendChild(type4);
      tabinput.appendChild(type5);
      tabinput.appendChild(type6);
    }
    else {
      if (content_records_head[i] == "field_group") {
        var tabinput = document.createElement("select");
        tabinput.setAttribute("id", content_records_head[i]);

        var group1 = document.createElement("option");
        group1.setAttribute("value", "_lb");
        if (fieldtoedit[0][content_records_head[i]] == "_lb") {
          group1.setAttribute("selected", "true");
        }
        var group1text = document.createTextNode("_lb");
        group1.appendChild(group1text);

        var group2 = document.createElement("option");
        group2.setAttribute("value", "_jm");
        if (fieldtoedit[0][content_records_head[i]] == "_jm") {
          group2.setAttribute("selected", "true");
        }
        var group2text = document.createTextNode("_jm");
        group2.appendChild(group2text);

        var group3 = document.createElement("option");
        group3.setAttribute("value", "_im");
        if (fieldtoedit[0][content_records_head[i]] == "_im") {
          group3.setAttribute("selected", "true");
        }
        var group3text = document.createTextNode("_im");
        group3.appendChild(group3text);

        var group4 = document.createElement("option");
        group4.setAttribute("value", "_ua");
        if (fieldtoedit[0][content_records_head[i]] == "_ua") {
          group4.setAttribute("selected", "true");
        }
        var group4text = document.createTextNode("_ua");
        group4.appendChild(group4text);

        var group5 = document.createElement("option");
        group5.setAttribute("value", "_pk");
        if (fieldtoedit[0][content_records_head[i]] == "_pk") {
          group5.setAttribute("selected", "true");
        }
        var group5text = document.createTextNode("_pk");
        group5.appendChild(group5text);

        tabinput.appendChild(group1);
        tabinput.appendChild(group2);
        tabinput.appendChild(group3);
        tabinput.appendChild(group4);
        tabinput.appendChild(group5);
      }
      else {
        if (content_records_head[i] == "field_view") {
          var tabinput = document.createElement("select");
          tabinput.setAttribute("id", content_records_head[i]);

          var view1 = document.createElement("option");
          view1.setAttribute("value", "main");
          if (fieldtoedit[0][content_records_head[i]] == "main") {
            view1.setAttribute("selected", "true");
          }
          var view1text = document.createTextNode("main");
          view1.appendChild(view1text);

          var view2 = document.createElement("option");
          view2.setAttribute("value", "details");
          if (fieldtoedit[0][content_records_head[i]] == "details") {
            view2.setAttribute("selected", "true");
          }
          var view2text = document.createTextNode("details");
          view2.appendChild(view2text);

          var view3 = document.createElement("option");
          view3.setAttribute("value", "pii");
          if (fieldtoedit[0][content_records_head[i]] == "pii") {
            view3.setAttribute("selected", "true");
          }
          var view3text = document.createTextNode("pii");
          view3.appendChild(view3text);

          var view4 = document.createElement("option");
          view4.setAttribute("value", "log");
          if (fieldtoedit[0][content_records_head[i]] == "log") {
            view4.setAttribute("selected", "true");
          }
          var view4text = document.createTextNode("log");
          view4.appendChild(view4text);

          tabinput.appendChild(view1);
          tabinput.appendChild(view2);
          tabinput.appendChild(view3);
          tabinput.appendChild(view4);
        }
        else {
          if (content_records_head[i] == "field_input") {
            var tabinput = document.createElement("select");
            tabinput.setAttribute("id", content_records_head[i]);

            var input1 = document.createElement("option");
            input1.setAttribute("value", "auto");
            if (fieldtoedit[0][content_records_head[i]] == "auto") {
              input1.setAttribute("selected", "true");
            }
            var input1text = document.createTextNode("auto");
            input1.appendChild(input1text);

            var input2 = document.createElement("option");
            input2.setAttribute("value", "write");
            if (fieldtoedit[0][content_records_head[i]] == "write") {
              input2.setAttribute("selected", "true");
            }
            var input2text = document.createTextNode("write");
            input2.appendChild(input2text);

            var input3 = document.createElement("option");
            input3.setAttribute("value", "picklist");
            if (fieldtoedit[0][content_records_head[i]] == "picklist") {
              input3.setAttribute("selected", "true");
            }
            var input3text = document.createTextNode("picklist");
            input3.appendChild(input3text);

            var input4 = document.createElement("option");
            input4.setAttribute("value", "file");
            if (fieldtoedit[0][content_records_head[i]] == "file") {
              input4.setAttribute("selected", "true");
            }
            var input4text = document.createTextNode("file");
            input4.appendChild(input4text);

            tabinput.appendChild(input1);
            tabinput.appendChild(input2);
            tabinput.appendChild(input3);
            tabinput.appendChild(input4);
          }
          else {
            if (
              (content_records_head[i] == "field_disabled")
              ||
              (content_records_head[i] == "field_static")
              ||
              (content_records_head[i] == "field_required")
              ||
              (content_records_head[i] == "watch_main")
              ) {
                var tabinput = document.createElement("select");
                tabinput.setAttribute("id", content_records_head[i]);

                var input1 = document.createElement("option");
                input1.setAttribute("value", true);
                if (fieldtoedit[0][content_records_head[i]] == true) {
                  input1.setAttribute("selected", "true");
                }
                var input1text = document.createTextNode(true);
                input1.appendChild(input1text);

                var input2 = document.createElement("option");
                input2.setAttribute("value", false);
                if (fieldtoedit[0][content_records_head[i]] == false) {
                  input2.setAttribute("selected", "true");
                }
                var input2text = document.createTextNode(false);
                input2.appendChild(input2text);

                tabinput.appendChild(input1);
                tabinput.appendChild(input2);
            }
            else {
              // input box
              var tabinput = document.createElement("input");
              tabinput.setAttribute("id", [content_records_head[i]]);
              if (i == 0) {
                tabinput.setAttribute("type", "number");
              }
              else {
                tabinput.setAttribute("type", "text");
              }
              tabinput.setAttribute("value", fieldtoedit[0][content_records_head[i]]);
              tabinput.setAttribute("style", "width: 90%; max-width: 200px");
              if (i == 0) {
                tabinput.setAttribute("disabled", "disabled");
              }
            }
          }
        }
      }
    }

    ymp_td.appendChild(tabinput);

    if (content_records_head[i] == "field_picklist") {

      var ymp_td = ymp_tr.insertCell(-1);                 // table cell
      var ymp_link = document.createElement("a");
      ymp_link.setAttribute("href", "#");
      ymp_link.setAttribute("onclick", "get_details("
        + "'picklist',"
        + "'./" + fieldtoedit[0][content_records_head[9]]
        + "','');");

      var ymp_options = document.createElement("img");
      ymp_options.setAttribute("src", "../img/ymp_options.png");
      ymp_options.setAttribute("style", "height: 15px; width: 15px;");
      ymp_options.setAttribute("title", "Details");
      ymp_options.setAttribute("id", "detailsbtn" + i);

      ymp_link.appendChild(ymp_options);
      ymp_td.appendChild(ymp_link);
    }
  }

  // finally add the newly created table with JSON data to a container
  var divContainer = document.getElementById("show_records_input1");
  divContainer.innerHTML = "";
  divContainer.appendChild(ymp_table);

  // add the buttons
  var btnContainer = document.getElementById('btnboardmodal');
  btnContainer.innerHTML = "";

  var btn_update = document.createElement("button");
  var btn_update_text = document.createTextNode("Update");
  btn_update.setAttribute("type", "button");
  btn_update.setAttribute("class", "redbtnmodal");
  btn_update.setAttribute("onclick", "updatefield('" + json_file + "','" + file_id  + "');");
  btn_update.appendChild(btn_update_text);

  var btn_cancel = document.createElement("button");
  var btn_cancel_text = document.createTextNode("Cancel");
  btn_cancel.setAttribute("type", "button");
  btn_cancel.setAttribute("class", "greenbtnmodal");
  btn_cancel.setAttribute("onclick", "document.getElementById('modal_edit').style.display='none';");
  btn_cancel.appendChild(btn_cancel_text);

  btnContainer.appendChild(btn_update);
  btnContainer.appendChild(btn_cancel);
}
// /editfield

// Update field after edit
function updatefield(json_file, file_id) {
  var JSONfile = top.getJSONfile("", json_file);

  var content_records = JSONfile[2];
  var content_records_head = JSONfile[3];

  // get the content to add

  // if header belongs of fields or picklist then
  if (
    (json_file.search("field") != -1)
    ||
    (json_file.search("picklist") != -1)
    ) {
      var new_records = '"' + content_records_head[0] + '"' + ':' +
      document.getElementById(content_records_head[0]).value;  }
  else {
    var new_records = '"' + content_records_head[0] + '"' + ':' +
    '"' + document.getElementById(content_records_head[0]).value + '"';
  }
  // go through all the data
  for (var j = 1; j < content_records_head.length; j++) {
    // specify the fields that you want to get no string value stored
    if (
      (content_records_head[j] == "field_disabled")
      ||
      (content_records_head[j] == "field_static")
      ||
      (content_records_head[j] == "field_required")
    ) {
      new_records = new_records + ", " + '"' + content_records_head[j] + '"' + ":" +
      document.getElementById(content_records_head[j]).value;
    }
    else {
      new_records = new_records + ", " + '"' + content_records_head[j] + '"' + ":" +
      '"' + document.getElementById(content_records_head[j]).value + '"';
    }
  }
  new_records = '{' + new_records + '}';
  new_records = JSON.parse(new_records);

  top.updateJSONfile(json_file, file_id, new_records);

  // disable all inputs
  for (var i = 0; i < content_records.length; i++) {
    try{
      document.getElementById(content_records_head[i]).disabled = "disabled";
    }
    catch(err) {
    }
  }

  // manage labels
  document.getElementById("addlblmodal").style.display = "none";
  document.getElementById("oklblmodal").style.display = "inline";
  document.getElementById("deletelblmodal").style.display = "none";

  // add the buttons
  var btnContainer = document.getElementById('btnboardmodal');
  btnContainer.innerHTML = "";

  var btn_ok = document.createElement("button");
  var btn_ok_text = document.createTextNode("Ok");
  btn_ok.setAttribute("type", "button");
  btn_ok.setAttribute("class", "greenbtnmodal");
  btn_ok.setAttribute("onclick", "okfield('" + json_file + "');");
  btn_ok.appendChild(btn_ok_text);

  btnContainer.appendChild(btn_ok);
}
// /updatefield

// Confirm fields editing
function okfield(json_file) {
  var script_callrefreshtable = document.createElement("script");
  script_callrefreshtable.innerHTML = "refreshtable('', '" + json_file + "');";
  document.body.appendChild(script_callrefreshtable);

  document.getElementById("addlblmodal").style.display = "inline";
  document.getElementById("oklblmodal").style.display = "none";
  document.getElementById("deletelblmodal").style.display = "none";

  document.getElementById("modal_edit").style.display = "none";
}
// /okfield

// Delete field - show field to delete
function deletefield(json_file, file_id) {
  // add the buttons
  var btnContainer = document.getElementById('btnboardmodal');
  btnContainer.innerHTML = "";

  var btn_confirmdelete = document.createElement("button");
  var btn_confirmdelete_text = document.createTextNode("Confirm Delete");
  btn_confirmdelete.setAttribute("type", "button");
  btn_confirmdelete.setAttribute("class", "redbtnmodal");
  btn_confirmdelete.setAttribute("onclick", "confirmdeletefield('" + json_file + "','" + file_id  + "');");
  btn_confirmdelete.appendChild(btn_confirmdelete_text);

  var btn_cancel = document.createElement("button");
  var btn_cancel_text = document.createTextNode("Cancel");
  btn_cancel.setAttribute("type", "button");
  btn_cancel.setAttribute("class", "greenbtnmodal");
  btn_cancel.setAttribute("onclick", "document.getElementById('modal_edit').style.display='none';");
  btn_cancel.appendChild(btn_cancel_text);

  btnContainer.appendChild(btn_confirmdelete);
  btnContainer.appendChild(btn_cancel);
}
// /deletefield

// Delete field confirmation - actual delete
function confirmdeletefield(json_file, file_id) {
  var JSONfile = top.getJSONfile("", json_file);

  var content_records = JSONfile[2];
  var content_records_head = JSONfile[3];

  top.deleteJSONrecord(json_file, file_id);

  // disable all inputs
  for (var i = 0; i < content_records_head.length; i++) {
    document.getElementById(content_records_head[i]).disabled = "disabled";
  }

  // manage labels
  document.getElementById("addlblmodal").style.display = "none";
  document.getElementById("oklblmodal").style.display = "none";
  document.getElementById("deletelblmodal").style.display = "inline";

  // add the buttons
  var btnContainer = document.getElementById('btnboardmodal');
  btnContainer.innerHTML = "";

  var btn_ok = document.createElement("button");
  var btn_ok_text = document.createTextNode("Ok");
  btn_ok.setAttribute("type", "button");
  btn_ok.setAttribute("class", "greenbtnmodal");
  btn_ok.setAttribute("onclick", "okfield('" + json_file + "');");
  btn_ok.appendChild(btn_ok_text);

  btnContainer.appendChild(btn_ok);
}
// /confirmdeletefield
