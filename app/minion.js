// declare variables
const electron = top.require("electron");
const BrowserWindow = electron.remote.BrowserWindow;
const path = top.require("path");
const url = top.require("url");

// init win
let win;

function createWindow(pg_area, pg_id, pg_view){

  // close any window left open
  try{win.close();}catch(err){}

  // Create browser window
  win = new BrowserWindow({
    width:580,
    height:720,

    minWidth: 580,
    minHeight: 720,

    icon: path.resolve(path.dirname('')) + "/app/img/ymp_icon_v_0_5.png",
    show:false
  });

  // remove default menu
  win.setMenu(null);

  win.loadURL(
      path.resolve(path.dirname('')) +
      "/app/pages/sub/pop_up.htm?" +
      pg_area + "&" + pg_id + "&" + pg_view);

  // Open devtools
  win.webContents.openDevTools();

  // show BrowserWindow when Chromium loads completely
    // other then .on there are: .show; .hide; .focus; .close
  win.on("ready-to-show", () => {
    win.show()
  });

  win.on("closed", () => {
    win = null;
  });
}
