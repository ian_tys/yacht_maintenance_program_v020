var url_data = window.location.toString();
var record_id = url_data.substring(url_data.indexOf("=")+1);
var file_extension = "";

// update profile with user requested details
function refreshprofile() {

  JSONfile = top.getJSONfile("tys_production/ympdb/field/field_user.json", "tys_production/ympdb/user.json");

  var content_fields = JSONfile[0];
  var content_fields_head = JSONfile[1];
  var content_records = JSONfile[2];
  var content_records_head = JSONfile[3];

  if (record_id != "") {
    // get Profile user
    var profile_user = top.getObjects(content_records, content_records_head[0], record_id);
  }

  // create dynamic table
  var ymp_table = document.createElement("table");

  var d_id =
    new Date().getFullYear()
    + ("0" + (parseInt(new Date().getMonth()) + 1)).slice(-2)
    + ("0" + (new Date().getDate())).slice(-2)
    + ("0" + (new Date().getHours())).slice(-2)
    + ("0" + (new Date().getMinutes())).slice(-2)
    + ("0" + (new Date().getSeconds())).slice(-2);

  // add JSON data to the table as rows
  for (var i = 0; i < content_fields.length; i++) {

    var ymp_tr = ymp_table.insertRow(-1); // table row
    if (content_fields[i][content_fields_head[6]] == "file") {
      // if field title is Photo then
      if (content_fields[i][content_fields_head[2]] == "Photo") {
        ymp_tr.setAttribute("id", "ymp_tr_photourl");
      }
    }

    var ymp_td = ymp_tr.insertCell(-1); // table cell1
    // show record field title
    ymp_td.innerHTML = content_fields[i][content_fields_head[2]];

    var ymp_td = ymp_tr.insertCell(-1); // table cell2

    if (content_fields[i][content_fields_head[2]].slice(-2) == "ID") {
      // input box
      var tabinput = document.createElement("input");
      if (record_id != "") {
         tabinput.setAttribute("value", record_id);
      }
      else {
        tabinput.setAttribute("value", d_id + content_fields[i][content_fields_head[4]]);
      }
    }
    else {
      if (content_fields[i][content_fields_head[2]] == "Username") {
        // input box
        var tabinput = document.createElement("input");

        if (record_id != "") {
          tabinput.setAttribute("value", profile_user[0][content_fields[i][content_fields_head[1]]]);

          // update Profile header with Username
          var divContainer = document.getElementById("profile_name_content");
          divContainer.innerHTML = profile_user[0][content_fields[i][content_fields_head[1]]] + " profile";
        }
      }
      else {
        if (content_fields[i][content_fields_head[2]] == "Title") {
          JSONfile = top.getJSONfile("", "tys_production/ympdb/picklist/user_title.json");

          var picklist_records = JSONfile[2];
          var picklist_records_head = JSONfile[3];

          var tabinput = document.createElement("select");
          tabinput.setAttribute("id", content_records_head[i]);

          for (var j = 0; j < picklist_records.length; j++) {

            var optionj = document.createElement("option");
            // save the picklist ID not text
            optionj.setAttribute("value", picklist_records[j][picklist_records_head[0]]);
            if (record_id != "") {
              if (profile_user[0][content_fields[i][content_fields_head[1]]] ==
                picklist_records[j][picklist_records_head[0]]) {
                  optionj.setAttribute("selected", "true");
              }
            }
            var optionjtext = document.createTextNode(picklist_records[j][picklist_records_head[1]]);
            optionj.appendChild(optionjtext);

            tabinput.appendChild(optionj);
          }
        }
        else {
          // input box
          var tabinput = document.createElement("input");
          if (record_id != "") {
            tabinput.setAttribute("value", profile_user[0][content_fields[i][content_fields_head[1]]]);
          }
        }
      }
    }

    tabinput.setAttribute("id", content_fields[i][content_fields_head[1]]);
    tabinput.setAttribute("type", content_fields[i][content_fields_head[3]]);
    if (record_id != "") {
      tabinput.setAttribute("disabled", "disabled");
    }
    else {
      if (content_fields[i][content_fields_head[7]] == true) {
        tabinput.setAttribute("disabled", "disabled");
      }
    }
    if (content_fields[i][content_fields_head[6]] == "file") {
      tabinput.setAttribute("placeholder", "Choose File");
    }

    ymp_td.appendChild(tabinput);

    // for url file upload - if field input is file then
    if (content_fields[i][content_fields_head[6]] == "file") {
      if (record_id == "") {
        var ymp_td = ymp_tr.insertCell(-1); // table cell

        var ymp_link = document.createElement("a");
        ymp_link.setAttribute("href", "#");
        ymp_link.setAttribute("onclick",
          "document.getElementById('file_" + content_fields[i][content_fields_head[2]] + "').click();");

        var ymp_options = document.createElement("img");
        ymp_options.setAttribute("src", "../img/ymp_options.png");
        ymp_options.setAttribute("style", "height: 15px; width: 15px;");
        ymp_options.setAttribute("title", "URL " + content_fields[i][content_fields_head[2]]);
        ymp_options.setAttribute("id", "url_" + content_fields[i][content_fields_head[2]] + "_btn");

        var ymp_input = document.createElement("input");
        ymp_input.setAttribute("type", "file");
        ymp_input.setAttribute("id", "file_" + content_fields[i][content_fields_head[2]]);
        file_extension = content_fields[i][content_fields_head[2]];
        ymp_input.addEventListener('change', handleObjectSelect, false);

        var ymp_output = document.createElement("output");
        ymp_output.setAttribute("id", "url_" + content_fields[i][content_fields_head[2]]);

        ymp_link.appendChild(ymp_options);
        ymp_td.appendChild(ymp_link);
        ymp_td.appendChild(ymp_input);
        ymp_td.appendChild(ymp_output);
      }
      else {
        if (profile_user[0][content_fields[i][content_fields_head[1]]] != "") {
          // upload photo to profile_pic
          var table_profile_pic = document.getElementById("cell_profile_pic");

          var profile_pic_update = document.getElementById("profile_pic");
          var profile_pic_src = profile_user[0][content_fields[i][content_fields_head[1]]]
          try{profile_pic_update.src = location.href.slice(8, 11) + profile_pic_src;}catch(err){}

          table_profile_pic.appendChild(profile_pic_update);
        }
      }
    }
    // else {
    //   // for picklist update - if field input is picklist then
    //   if (content_fields[i][content_fields_head[6]] == "picklist") {
    //     var ymp_td = ymp_tr.insertCell(-1); // table cell
    //
    //     var ymp_link = document.createElement("a");
    //     ymp_link.setAttribute("href", "#");
    //     ymp_link.setAttribute("onclick", "document.location.replace('settings.htm');");
    //
    //     var ymp_options = document.createElement("img");
    //     ymp_options.setAttribute("src", "../img/ymp_options.png");
    //     ymp_options.setAttribute("style", "height: 15px; width: 15px;");
    //     ymp_options.setAttribute("title", "URL Photo");
    //     ymp_options.setAttribute("id", "url_photo_btn");
    //
    //     ymp_link.appendChild(ymp_options);
    //     ymp_td.appendChild(ymp_link);
    //   }
    // }
  }

  // finally add the newly created table with JSON data to a container
  var divContainer = document.getElementById("profile_main_content");
  divContainer.innerHTML = "";
  divContainer.appendChild(ymp_table);

  // keep just the required buttons
  if (record_id != "")
  {
    document.getElementById("addbtnprofile").style = "display: none;"
    document.getElementById("editbtnprofile").style = "display: inline;"
    document.getElementById("updatebtnprofile").style = "display: none;"
    document.getElementById("deletebtnprofile").style = "display: inline;"
    document.getElementById("confirmbtnprofile").style = "display: none;"
    document.getElementById("rcancelbtnprofile").style = "display: none;"
    document.getElementById("gcancelbtnprofile").style = "display: none;"
  }
  else {
    document.getElementById("addbtnprofile").style = "display: inline;"
    document.getElementById("editbtnprofile").style = "display: none;"
    document.getElementById("updatebtnprofile").style = "display: none;"
    document.getElementById("deletebtnprofile").style = "display: none;"
    document.getElementById("confirmbtnprofile").style = "display: none;"
    document.getElementById("rcancelbtnprofile").style = "display: inline;"
    document.getElementById("gcancelbtnprofile").style = "display: none;"
  }
}
// /refreshprofile

// Collect event for the Photo handler
function handleObjectSelect(evt) {
  var files = evt.target.files; // FileList object

  document.getElementById("url_" + file_extension).innerHTML = files[0]["path"];

  if (file_extension == "Photo") {
    // upload photo to profile_pic
    var table_profile_pic = document.getElementById("cell_profile_pic");

    var profile_pic_update = document.getElementById("profile_pic");
    var profile_pic_src = files[0]["path"];
    profile_pic_update.src = profile_pic_src;

    table_profile_pic.appendChild(profile_pic_update);
  }
}

// Add a new profile
function addprofile() {

  JSONfile = top.getJSONfile("tys_production/ympdb/field/field_user.json", "tys_production/ympdb/user.json");

  var content_fields = JSONfile[0];
  var content_fields_head = JSONfile[1];

  var new_records = '"' + content_fields[0][content_fields_head[1]] + '"' + ':' +
    '"' + document.getElementById(content_fields[0][content_fields_head[1]]).value + '"';
  for (var j = 1; j < content_fields.length; j++) {
    if (content_fields[j][content_fields_head[6]] == "file") {
      var url_file = document.getElementById("url_" + content_fields[j][content_fields_head[2]]).value;
      var url_transfer = ""

      if (
        (url_file != "")
        &&
        (url_file != undefined)
      ) {
        url_transfer = document.getElementById("file_" + content_fields[j][content_fields_head[2]]).value;
        url_transfer = url_transfer.substring(url_transfer.indexOf("\\fakepath\\")+10);
        url_transfer = content_fields[j][content_fields_head[9]] + "/" + url_transfer;

        var fs = top.require('fs');

        fs.createReadStream(url_file).pipe(
          fs.createWriteStream(location.href.slice(8, 11) + url_transfer));

        url_transfer.replace("\\", "/");

        // if (url_photo != "") {
        //   console.log("transfer from: " + url_photo + "to: " + url_transfer);
        // }
      }

      new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
      '"' + url_transfer + '"';
    }
    else {
      // console.log(content_fields[j][content_fields_head[1]]);
      new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
      '"' + document.getElementById(content_fields[j][content_fields_head[1]]).value + '"'
    }
  };
  new_records = '{' + new_records + '}';

  new_records = JSON.parse(new_records);

  document.getElementById("modal_load").style.display="block";

  top.addJSONrecord("tys_production/ympdb/user.json", new_records);

  setTimeout(
    function(){
      document.location.replace("employee.htm");
    }, 2000);
}
// /addprofile

// Edit the profile
function editprofile() {

  JSONfile = top.getJSONfile("tys_production/ympdb/field/field_user.json", "tys_production/ympdb/user.json");

  var content_fields = JSONfile[0];
  var content_fields_head = JSONfile[1];

  // add JSON data to the table as rows
  for (var i = 0; i < content_fields.length; i++) {
    // if field is a password then
    if (content_fields[i][content_fields_head[3]] == "password") {
      document.getElementById(content_fields[i][content_fields_head[1]]).type = "password";
    }

    // if field is not set to disabled then
    if (content_fields[i][content_fields_head[7]] != true) {
      document.getElementById(content_fields[i][content_fields_head[1]]).disabled = false;
    }

    // for url file upload - if field input is file then
    if (content_fields[i][content_fields_head[6]] == "file") {

      if (content_fields[i][content_fields_head[2]] == "Photo") {
        var ymp_tr = document.getElementById("ymp_tr_photourl");
      }
      var ymp_td = ymp_tr.insertCell(-1); // table cell

      var ymp_link = document.createElement("a");
      ymp_link.setAttribute("href", "#");
      ymp_link.setAttribute("onclick",
        "document.getElementById('file_" + content_fields[i][content_fields_head[2]] + "').click();");

      var ymp_options = document.createElement("img");
      ymp_options.setAttribute("src", "../img/ymp_options.png");
      ymp_options.setAttribute("style", "height: 15px; width: 15px;");
      ymp_options.setAttribute("title", "URL " + content_fields[i][content_fields_head[2]]);
      ymp_options.setAttribute("id", "url_" + content_fields[i][content_fields_head[2]] + "_btn");

      var ymp_input = document.createElement("input");
      ymp_input.setAttribute("type", "file");
      ymp_input.setAttribute("id", "file_" + content_fields[i][content_fields_head[2]]);
      file_extension = content_fields[i][content_fields_head[2]];
      ymp_input.addEventListener('change', handleObjectSelect, false);

      var ymp_output = document.createElement("output");
      ymp_output.setAttribute("id", "url_" + content_fields[i][content_fields_head[2]]);

      ymp_link.appendChild(ymp_options);
      ymp_td.appendChild(ymp_link);
      ymp_td.appendChild(ymp_input);
      ymp_td.appendChild(ymp_output);

      ymp_tr.appendChild(ymp_td);

    }
  }

  // keep just the required buttons
  document.getElementById("editbtnprofile").style = "display: none;"
  document.getElementById("deletebtnprofile").style = "display: none;"
  document.getElementById("updatebtnprofile").style = "display: inline;"
  document.getElementById("gcancelbtnprofile").style = "display: inline;"
}
// /editprofile

// Update the profile
function updateprofile() {

  JSONfile = top.getJSONfile("tys_production/ympdb/field/field_user.json", "tys_production/ympdb/user.json");

  var content_fields = JSONfile[0];
  var content_fields_head = JSONfile[1];

  var new_records = '"' + content_fields[0][content_fields_head[1]] + '"' + ':' +
    '"' + document.getElementById(content_fields[0][content_fields_head[1]]).value + '"';
  for (var j = 1; j < content_fields.length; j++) {
    // for url file upload - if field input is file then
    if (content_fields[j][content_fields_head[6]] == "file") {

      var url_file = document.getElementById("url_" + content_fields[j][content_fields_head[2]]).value;
      var url_transfer = "";

      if (document.getElementById("file_"
        + content_fields[j][content_fields_head[2]]).value != "") {
        if (
          (url_file != "")
          ||
          (url_file != undefined)
        ) {
          url_transfer = document.getElementById("file_"
            + content_fields[j][content_fields_head[2]]).value;
          url_transfer = url_transfer.substring(url_transfer.indexOf("\\fakepath\\")+10);
          url_transfer = content_fields[j][content_fields_head[9]] + "/" + url_transfer;

          var fs = top.require('fs');

          fs.createReadStream(url_file).pipe(
            fs.createWriteStream(location.href.slice(8, 11) + url_transfer));

          url_transfer.replace("\\", "/");
        }
        else {
          if (document.getElementById(content_fields[j][content_fields_head[1]]).value != "") {
            url_transfer = document.getElementById(content_fields[j][content_fields_head[1]]).value;
          }
        }
      }
      else {
        url_transfer = document.getElementById(content_fields[j][content_fields_head[1]]).value;
      }

      new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
      '"' + url_transfer + '"'
    }
    else {
      // console.log(content_fields[j][content_fields_head[1]]);
      new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
      '"' + document.getElementById(content_fields[j][content_fields_head[1]]).value + '"'
    }
  };

  new_records = '{' + new_records + '}';
  new_records = JSON.parse(new_records);

  document.getElementById("modal_load").style.display="block";

  top.updateJSONfile("tys_production/ympdb/user.json", record_id, new_records);

  setTimeout(
    function(){
      document.location.replace("profile.htm?var=" + record_id);
    }, 2000);
}
// /updateprofile

// Make sure user wants to really delete profile
function deleteprofile() {
  document.getElementById("editbtnprofile").style = "display: none;"
  document.getElementById("deletebtnprofile").style = "display: none;"
  document.getElementById("confirmbtnprofile").style = "display: inline;"
  document.getElementById("gcancelbtnprofile").style = "display: inline;"
}
// /deleteprofile

// Delete the profile
function confirmdeleteprofile() {
  document.getElementById("modal_load").style.display="block";

  top.deleteJSONrecord("tys_production/ympdb/user.json", record_id);

  setTimeout(
    function(){
      document.location.replace("employee.htm");
    }, 2000);
}
// /confirmdeleteprofile
