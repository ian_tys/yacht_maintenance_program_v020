// Load the user's main watchkeeper
function refreshwatchkeeper() {

    try{
      JSONfile = top.getJSONfile("", "tys_production/ympdb/watchkeeper/watchkeeper.json");

      var content_records = JSONfile[2];
      var content_records_head = JSONfile[3];

      var watchkeeper_record = top.getObjects(content_records, content_records_head[2], "yes");

      document.getElementById("main_watchkeeper").innerHTML = watchkeeper_record[0]["watch_content"];
    }
    catch(err) {}

}

// Activate the edit main watchkeeper
function edit_watchkeeper() {

  if (document.getElementById("editwatchkeeperbtn").style.display != "none") {
    document.getElementById("editwatchkeeperbtn").style.display = "none";
    document.getElementById("savewatchkeeperbtn").style.display = "inline";

    document.getElementById("addcheckbox_div").style.display = "inline";

    document.getElementById("main_watchkeeper").contentEditable = "true";
    // change content editor to font color black
    document.getElementById("main_watchkeeper").style.color = "#000";
    // change content editor to background-color white
    document.getElementById("main_watchkeeper").style.backgroundColor = "#fff";
  }

}

// Save the edited Watchkeeper
function save_watchkeeper() {

  JSONfile = top.getJSONfile("", "tys_production/ympdb/watchkeeper/watchkeeper.json");

  var content_records = JSONfile[2];
  var content_records_head = JSONfile[3];

  var watchkeeper_record = top.getObjects(content_records, content_records_head[2], "yes");

  var content_update = document.getElementById("main_watchkeeper").innerHTML;
  content_update = content_update.replace(/"/g, "'");

  try {
    var new_records = '"' + content_records_head[0] + '"' + ':' +
      '"' + watchkeeper_record[0][content_records_head[0]] + '"';
    for (var i = 1; i < content_records_head.length; i++) {
      if (content_records_head[i] == "watch_content") {
        new_records = new_records + ", " + '"' + content_records_head[i] + '"' + ":" +
        '"' + content_update + '"';
      }
      else {
        // console.log(content_fields[j][content_fields_head[1]]);
        new_records = new_records + ", " + '"' + content_records_head[i] + '"' + ":" +
        '"' + watchkeeper_record[0][content_records_head[i]] + '"'
      }
    };
    new_records = '{' + new_records + '}';
    new_records = JSON.parse(new_records);

    if (document.getElementById("savewatchkeeperbtn").style.display != "none") {
      document.getElementById("editwatchkeeperbtn").style.display = "inline";
      document.getElementById("savewatchkeeperbtn").style.display = "none";

      document.getElementById("addcheckbox_div").style.display = "none";

      document.getElementById("main_watchkeeper").contentEditable = "false";
      // change content editor to font color 10% dark sea blue
      document.getElementById("main_watchkeeper").style.color = "#002333";
      // change content editor to original background-color 75% dark sea blue
      document.getElementById("main_watchkeeper").style.backgroundColor = "#80d7ff";
    }

    top.updateJSONfile("tys_production/ympdb/watchkeeper/watchkeeper.json", watchkeeper_record[0][content_records_head[0]], new_records);
  }
  catch(err) {
    alert("Save cannot be performed! Make sure Watch Keeper is properly setup!");
  }
}

// Add a checkbox to Watchkeeper
function add_checkbox() {
  var divwatchcontent = document.getElementById("main_watchkeeper");

  var watchcheckbox = document.createElement("input");
  watchcheckbox.setAttribute("type", "checkbox");

  divwatchcontent.appendChild(watchcheckbox);
}
