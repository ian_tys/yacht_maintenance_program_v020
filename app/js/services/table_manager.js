// Declare global variables - Usefull to edit data selected
var content_fields = [];
var content_fields_head = [];
var content_records = [];
var content_records_head = [];
var recordtoedit = [];
var edit_json_fields = "";
var edit_json_file = "";

// pop-up variables
var field_name = "";

// Create a new dynamic table
function refreshtable(json_fields, json_file) {

  // get all the related data from JSON Files
  JSONfile = top.getJSONfile(json_fields, json_file);

  if (json_fields != "") {
    content_fields = JSONfile[0];
  }

  content_records = JSONfile[2];
  content_records_head = JSONfile[3];
  // /get all the related data from JSON Files

  // for HTML header
  var records_head = [" "];

  // if record in edit mode
  if (json_fields != "") {
    var records_view = [" "];
    var records_input = [" "];
    var records_url = [" "];
    var records_type = [" "];
    var records_picklist = [" "]

    for (var i = 0; i < content_fields.length; i++) {
      if (content_fields[i]["field_view"] == "main") {
        records_head.push(content_fields[i]["field_title"]);
        records_view.push(content_fields[i]["field"]);
        records_input.push(content_fields[i]["field_input"]);
        records_url.push(content_fields[i]["field_url"]);
        records_type.push(content_fields[i]["field_type"]);
        records_picklist.push(content_fields[i]["field_picklist"]);
      }
    }
  }
  // if no record in edit mode
  else {
    for (var i = 0; i < content_records.length; i++) {
      for (var key in content_records[i]) {
        if (records_head.indexOf(key) === -1) {
            records_head.push(key);
        }
      }
    }
  }

  // create dynamic input for search
  var ymp_search = document.createElement("input");
  // set input style and function
  ymp_search.setAttribute("id", "search_input");
  ymp_search.setAttribute("type", "text");
  ymp_search.setAttribute("onkeyup", "searchtable();");
  ymp_search.setAttribute("placeholder", "Search for data...");
  ymp_search.setAttribute("title", "Type in your search");

  var divContainer = document.getElementById("search_records");
  divContainer.innerHTML = "";
  divContainer.appendChild(ymp_search);

  // create dynamic table
  var ymp_toptable = document.createElement("table"); // table
  // set table style and function
  ymp_toptable.setAttribute("id", "ymp_toptable");
  ymp_toptable.setAttribute("class", "maindatatable");

  // create dynamic thead
  var ymp_thead = ymp_toptable.createTHead(); // table thead

  // create hmtl table header row using the extracted header above
  var ymp_tr = ymp_thead.insertRow(-1); // table row

  for (var i = 0; i < records_head.length; i++) {
      var ymp_th = document.createElement("th");  // table header
      ymp_th.innerHTML = records_head[i];
      // set header border style
      if ((i != 0) && (i != records_head.length-1)) {
        ymp_th.setAttribute("class", "dtable_hborder");
      }
      if (i != 0) {
        ymp_th.setAttribute("onclick", "sortTable(" + i + ", 'asc');");
      }
      ymp_tr.appendChild(ymp_th);
  }

  // create dynamic table
  var ymp_table = document.createElement("table");
  // set table style and function
  ymp_table.setAttribute("id", "ymp_datatable");
  ymp_table.setAttribute("class", "maindatatable");

  // if record in edit mode
  if (json_fields != "") {
    // for HTML header
    var records_head = [" "];

    for (var i = 0; i < content_records.length; i++) {
        for (var key in content_records[i]) {
            if (records_head.indexOf(key) === -1) {
                records_head.push(key);
            }
        }
    }
  }

  // create dynamic tbody
  var ymp_tbody = document.createElement("tbody");

  // add JSON data to the table as rows
  for (var i = 0; i < content_records.length; i++) {

    ymp_tr = ymp_table.insertRow(-1); // table row

    // add the details button for the edit option
    var ymp_td = ymp_tr.insertCell(-1); // table cell
    var ymp_link = document.createElement("a");
    ymp_link.setAttribute("href", "#");
    ymp_link.setAttribute("onclick",
      "get_details('"
      + json_fields
      + "','"
      + json_file
      + "','"
      + content_records[i][content_records_head[0]]
      + "');");

    var ymp_options = document.createElement("img");
    ymp_options.setAttribute("src", "../img/ymp_options.png");
    ymp_options.setAttribute("style", "display: none; height: 15px; width: 15px;");
    ymp_options.setAttribute("title", "Details");
    ymp_options.setAttribute("id", "detailsbtn" + i);

    ymp_link.appendChild(ymp_options);
    ymp_td.appendChild(ymp_link);

    // build the table

    // if record in edit mode
    if (json_fields != "") {
      records_head = records_view;
    }

    for (var j = 1; j < records_head.length; j++) {
      var ymp_td = ymp_tr.insertCell(-1); // table cell

      // if record in edit mode
      if (json_fields != "") {
        if (records_input[j] == "picklist") {
          JSONfile = top.getJSONfile("", records_url[j]);
          var content_picklistrecords = JSONfile[2];

          if (records_type[j] == "text") {
            // get the related record
            recordtoedit = top.getObjects(content_picklistrecords, records_picklist[j] + "_id",
              content_records[i][records_head[j]])
            // get the related record
            ymp_td.innerHTML = recordtoedit[0][records_view[j]];
          }
          else {
            if (records_type[j] == "boolean") {
              if (content_picklistrecords[0][[records_picklist[j]] + "_value"] ==
                content_records[i][records_head[j]]) {
                  ymp_td.innerHTML = content_picklistrecords[0][records_picklist[j] + "_text"];
              }
              else {
                if (content_picklistrecords[1][[records_picklist[j]] + "_value"] ==
                  content_records[i][records_head[j]]) {
                    ymp_td.innerHTML = content_picklistrecords[1][records_picklist[j] + "_text"];
                }
              }
            }
          }
        }
        // if not picklist
        else {
          if (records_type[j] == "datetime-local") {
            var datetime_format =
              content_records[i][records_head[j]].toString();

            datetime_format =
              datetime_format.slice(0, 4)
              + "-"
              + datetime_format.slice(4, 6)
              + "-"
              + datetime_format.slice(6, 8)
              + "T"
              + datetime_format.slice(8, 10)
              + ":"
              + datetime_format.slice(10, 12)
              + ":"
              + datetime_format.slice(12, 14);

            ymp_td.innerHTML = datetime_format;
          }
          else {
            ymp_td.innerHTML = content_records[i][records_head[j]];
          }
        }
      }
      // if no record in edit mode
      else {
          ymp_td.innerHTML = content_records[i][records_head[j]];
      }
      // set the cells border style
      if (j != records_head.length-1) {
        ymp_td.setAttribute("class", "dtable_cborder");
      }
    }
    ymp_tbody.appendChild(ymp_tr);
  }

  ymp_table.appendChild(ymp_tbody);

  // finally add the newly created table with JSON data to a container
  var divContainer = document.getElementById("show_main_records");
  divContainer.innerHTML = "";
  divContainer.appendChild(ymp_table);

  var divContainer = document.getElementById("top_main_records");
  divContainer.innerHTML = "";
   if (ymp_table.offsetHeight < 200) {
    divContainer.style = "padding-right: 0px;";
   }
  divContainer.appendChild(ymp_toptable);

  // table default sorting order
  if (
    (json_file.search("field") != -1)
    ||
    (json_file.search("picklist") != -1)
    ) {
      // desc is calling function twice
      sortTable(1, 'asc'); sortTable(1, 'asc');
  }
  else {
    sortTable(1, 'asc');
  }

  // show number of records
  var divContainer = document.getElementById("number_of_records");
  divContainer.innerHTML = "Count: " + content_records.length;
}
// /refreshtable

// Sort table header functionality
function sortTable(n, dir) {
  var table, rows, switching, i, x, y, shouldSwitch, switchcount = 0;

  // set table default sorting order
  //var dir = "asc";

  table = document.getElementById("ymp_datatable");
  switching = true;
  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.getElementsByTagName("TR");
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 0; i < (rows.length - 1); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[n].innerHTML.toLowerCase();
      if (x < 10) {x = ("0" + x).slice(-2)}
      y = rows[i + 1].getElementsByTagName("TD")[n].innerHTML.toLowerCase();
      if (y < 10) {y = ("0" + y).slice(-2)}
      /*check if the two rows should switch place,
      based on the direction, asc or desc:*/
      if (dir == "asc") {
        if (x > y) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      } else if (dir == "desc") {
        if (x < y) {
          //if so, mark as a switch and break the loop:
          shouldSwitch= true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      //Each time a switch is done, increase this count by 1:
      switchcount ++;
    }
    else {
      /*If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again.*/
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}
// /sortTable

// Go through table data and output findings
function searchtable() {
  var input, filter, table, tr, td, i;

  input = document.getElementById("search_input");
  filter = input.value.toUpperCase();
  table = document.getElementById("ymp_datatable");
  tr = table.getElementsByTagName("tr");

  for (i = 0; i < tr.length; i++) {
    if (tr[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
      tr[i].style.display = "";
    } else {
      tr[i].style.display = "none";
    }
  }
}
// /searchtable

// Create a new dynamic form
function refreshform(json_fields, json_file, file_id) {

  // get all the related data from JSON Files
  var JSONfile = top.getJSONfile(json_fields, json_file);

  content_fields = JSONfile[0];
  content_fields_head = JSONfile[1];
  content_records = JSONfile[2];
  content_records_head = JSONfile[3];
  // /get all the related data from JSON Files

  // get the related record to edit
  recordtoedit = top.getObjects(content_records, content_records_head[0], file_id);

  edit_json_fields = json_fields;
  edit_json_file = json_file;

  // populate form
  populate_division(file_id);
}
// /refreshform

// Fill dynamic fields in the division accordingly
function populate_division(file_id) {

  // count the number of dynamic fields
  var static_count = 0;
  for (var j = 0; j < content_fields.length; j++) {
    if (content_fields[j].field_static == false) {
      static_count = static_count + 1;
    }
  }

  // create dynamic tables
  var ymp_table1 = document.createElement("table"); // table 1
  var ymp_table2 = document.createElement("table"); // table 2
  var ymp_table3 = document.createElement("table"); // table 3

  var ymp_table1_count = 0;
  var ymp_table2_count = 0;
  var ymp_table3_count = 0;

  if (static_count == 1) {
    ymp_table1_count = 1;
  }
  else {
    if (static_count > 1) {
      ymp_table1_count = Math.round(static_count/3);
      ymp_table2_count = Math.round(static_count/3);
      ymp_table3_count = Math.round(static_count/3);
    }
  }

  // add JSON data to the table as rows - go through all the record fields
  for (var i = 0; i < content_fields.length; i++) {

    // if field is not static then
    if (content_fields[i][content_fields_head[10]] != true) {

      // distribute fields according to count and number of divs
      if (ymp_table1_count != 0) {
        var ymp_tr = ymp_table1.insertRow(-1); // table row
        ymp_table1_count = ymp_table1_count - 1;
      }
      else {
        if (ymp_table2_count != 0) {
          var ymp_tr = ymp_table2.insertRow(-1); // table row
          ymp_table2_count = ymp_table2_count - 1;
        }
        else {
          if (ymp_table3_count != 0) {
            var ymp_tr = ymp_table3.insertRow(-1); // table row
            ymp_table3_count = ymp_table3_count - 1;
          }
        }
      }

      // id row so that we can hide and unhide accordingly
      ymp_tr.setAttribute("id", "tr_" + content_fields[i][content_fields_head[1]]);

      var ymp_td = ymp_tr.insertCell(-1); // table cell1
      // show record field title
      ymp_td.innerHTML = content_fields[i][content_fields_head[2]];

      var ymp_td = ymp_tr.insertCell(-1); // table cell2

      // if field input is picklist then
      if (content_fields[i][content_fields_head[6]] == "picklist") {

        // collect picklist records
        JSONfile = top.getJSONfile("", content_fields[i][content_fields_head[9]]);

        var picklist_records = JSONfile[2];
        var picklist_records_head = JSONfile[3];
        // /collect picklist records

        // if field type is boolean then
        if (content_fields[i][content_fields_head[3]] == "boolean") {
          var tabinput = document.createElement("select"); // table select drop-down
          tabinput.setAttribute("id", content_fields[i][content_fields_head[1]]);
          tabinput.setAttribute("class", "default_txtbox");

          for (var j = 0; j < picklist_records.length; j++) {

            var optionj = document.createElement("option"); // table select drop- down option
            optionj.setAttribute("value", picklist_records[j][picklist_records_head[1]]);

            // if record in edit mode
            if (file_id != "") {
              if (recordtoedit[0][content_fields[i][content_fields_head[1]]] ==
                picklist_records[j][picklist_records_head[1]]) {
                  optionj.setAttribute("selected", "true");
              }
              tabinput.setAttribute("disabled", "disabled");
            }

            var optionjtext = document.createTextNode(picklist_records[j][picklist_records_head[2]]);
            optionj.appendChild(optionjtext);
            tabinput.appendChild(optionj);

            ymp_td.appendChild(tabinput);
          }
        }
        else {
          // if field type is text then
          if (content_fields[i][content_fields_head[3]] == "text") {
            var tabinput = document.createElement("select"); // table select drop-down
            tabinput.setAttribute("id", content_fields[i][content_fields_head[1]]);
            tabinput.setAttribute("class", "default_txtbox");

            for (var j = 0; j < picklist_records.length; j++) {

              var optionj = document.createElement("option"); // table select drop-down option
              optionj.setAttribute("value", picklist_records[j][picklist_records_head[0]]);

              // if record in edit mode
              if (file_id != "") {

                if (recordtoedit[0][content_fields[i][content_fields_head[1]]] ==
                  picklist_records[j][picklist_records_head[0]]) {
                    optionj.setAttribute("selected", "true");
                }
                tabinput.setAttribute("disabled", "disabled");

                // if empty record or undefined then hide row in view mode
                // if (
                //   (recordtoedit[0][content_fields[i][content_fields_head[1]]] == 1)
                //   ||
                //   (recordtoedit[0][content_fields[i][content_fields_head[1]]] === undefined)
                // ) {
                //   ymp_tr.setAttribute("style", "display: none;");
                // }

              }

              var optionjtext = document.createTextNode(picklist_records[j][picklist_records_head[1]]);
              optionj.appendChild(optionjtext);
              tabinput.appendChild(optionj);

              ymp_td.appendChild(tabinput);
            }

            var ymp_link = document.createElement("a");
            ymp_link.setAttribute("href", "#");

            // for exceptional picklists that use files from other main pages

            // if picklist is part of user then
            if (content_fields[i][content_fields_head[9]].search("user") != -1) {

              // if picklist is part of job then
              if (content_fields[0][content_fields_head[1]].search("job") != -1) {

                ymp_link.setAttribute("onclick", "get_details("
                + "'main_picklist',"
                + "'./" + content_fields[i][content_fields_head[9]]
                + "','job_exception');");

              }
              // if picklist is not part of job then assign default onclick
              else {

                ymp_link.setAttribute("onclick", "get_details("
                + "'main_picklist',"
                + "'./" + content_fields[i][content_fields_head[9]]
                + "','');");

              }

            }
            // if picklist is not part of user then assign default onclick
            else {

              ymp_link.setAttribute("onclick", "get_details("
                + "'main_picklist',"
                + "'./" + content_fields[i][content_fields_head[9]]
                + "','');");

            }

            ymp_link.setAttribute("id", content_fields[i][content_fields_head[1]] + "_link");

            var ymp_options = document.createElement("img");
            ymp_options.setAttribute("src", "../img/ymp_options.png");
            ymp_options.setAttribute("style", "display: none; height: 15px; width: 15px;");
            ymp_options.setAttribute("title", "Update");
            ymp_options.setAttribute("id", content_fields[i][content_fields_head[1]] + "_details");

            ymp_link.appendChild(ymp_options);
            ymp_td.appendChild(ymp_link);
          }
        }
      }
      // else if field input is not picklist then
      else {
        // if field input is file then
        if (content_fields[i][content_fields_head[6]] == "file") {
          var ymp_div = document.createElement("div"); // table divider
          // set style to background-color white and font color black
          ymp_div.setAttribute("class", "div_file")

          var ymp_link = document.createElement("a"); // table link
          ymp_link.setAttribute("href", "#");
          ymp_link.setAttribute("onclick",
            "document.getElementById('input_" + content_fields[i][content_fields_head[1]] + "').click();");

          var ymp_options = document.createElement("img"); // table image
          ymp_options.setAttribute("src", "../img/ymp_options.png");
          ymp_options.setAttribute("style", "height: 15px; width: 15px;");
          if (file_id != "") {
            ymp_options.setAttribute("style", "display: none;");
          }
          ymp_options.setAttribute("title", "URL " + content_fields[i][content_fields_head[2]]);
          ymp_options.setAttribute("id", "options_" + content_fields[i][content_fields_head[1]] + "_btn");

          var ymp_input = document.createElement("input"); // table input
          ymp_input.setAttribute("type", "file");
          ymp_input.setAttribute("id", "input_" + content_fields[i][content_fields_head[1]]);
          ymp_input.setAttribute("class", "file_pic");
          ymp_input.addEventListener('change', handleObjectSelect, false);

          var ymp_output = document.createElement("output"); // table output
          ymp_output.setAttribute("id", content_fields[i][content_fields_head[1]]);
          ymp_output.innerHTML = "(empty)"

          // if record in edit mode
          if (file_id != "") {
            var ymp_img = document.createElement("img"); // table image

            // if field is not empty or not undefined
            if (
              (recordtoedit[0][content_fields[i][content_fields_head[1]]] != "")
              &&
              (recordtoedit[0][content_fields[i][content_fields_head[1]]] !== undefined)
            ) {
              ymp_output.innerHTML = ""
              ymp_output.alt = recordtoedit[0][content_fields[i][content_fields_head[1]]];

              ymp_img.setAttribute("src",
                autochooseicon(recordtoedit[0][content_fields[i][content_fields_head[1]]]));

              ymp_img.setAttribute("onclick", "opnfile('" + location.href.slice(8, 11) +
                recordtoedit[0][content_fields[i][content_fields_head[1]]]
                + "');");
            }
            // if field is empty
            // else {
            //   ymp_tr.setAttribute("style", "display: none;");
            // }

            ymp_img.setAttribute("style", "cursor: pointer; margin-top: 6px; max-height: 50px; max-width: 75px;");
            ymp_img.setAttribute("id", "img_" + content_fields[i][content_fields_head[1]]);

            var ymp_remove_link = document.createElement("a");
            ymp_remove_link.setAttribute("href", "#");
            ymp_remove_link.setAttribute("onclick",
              "removefile('" + content_fields[i][content_fields_head[1]] + "')"
            );

            var ymp_remove_img = document.createElement("img");
            ymp_remove_img.setAttribute("id", "remove_" + content_fields[i][content_fields_head[1]]);
            ymp_remove_img.setAttribute("src", "../img/ymp_removefile.png");
            ymp_remove_img.setAttribute("style", "display: none;");
            ymp_remove_img.setAttribute("title", "Cancel selected item");

            ymp_remove_link.appendChild(ymp_remove_img);

            ymp_div.appendChild(ymp_img);
            ymp_div.appendChild(ymp_remove_link);
          }
          // /if record in edit mode

          ymp_link.appendChild(ymp_options);
          ymp_div.appendChild(ymp_link);
          ymp_div.appendChild(ymp_input);
          ymp_div.appendChild(ymp_output);
          ymp_td.appendChild(ymp_div);
        }
        // if field input is not even file then
        else {

          var tabinput = document.createElement("input"); // table input
          tabinput.setAttribute("class", "default_txtbox");

          // if record in edit mode
          if (file_id != "") {

            if (content_fields[i][content_fields_head[3]] == "datetime-local") {
              var datetime_format =
                recordtoedit[0][content_fields[i][content_fields_head[1]]].toString();

              if ((datetime_format.slice(12, 14) == "00") || (datetime_format.slice(12, 14) == "")) {
                datetime_format =
                datetime_format.slice(0, 4)
                + "-"
                + datetime_format.slice(4, 6)
                + "-"
                + datetime_format.slice(6, 8)
                + "T"
                + datetime_format.slice(8, 10)
                + ":"
                + datetime_format.slice(10, 12)
                + ":00"
              }
              else {
                datetime_format =
                datetime_format.slice(0, 4)
                + "-"
                + datetime_format.slice(4, 6)
                + "-"
                + datetime_format.slice(6, 8)
                + "T"
                + datetime_format.slice(8, 10)
                + ":"
                + datetime_format.slice(10, 12)
                + ":"
                + datetime_format.slice(12, 14);
              }

              tabinput.setAttribute("value", datetime_format);
            }
            else {
                if (content_fields[i][content_fields_head[3]] == "date") {
                  var date_format =
                    recordtoedit[0][content_fields[i][content_fields_head[1]]].toString();

                  date_format =
                    date_format.slice(0, 4)
                    + "-"
                    + date_format.slice(4, 6)
                    + "-"
                    + date_format.slice(6, 8);

                  tabinput.setAttribute("value", date_format);
                }
                else {
                  if (content_fields[i][content_fields_head[3]] == "time") {
                    var time_format =
                      recordtoedit[0][content_fields[i][content_fields_head[1]]].toString();

                    if (time_format.length == 3) {
                      time_format = "0" + time_format;
                    }

                    time_format =
                      time_format.slice(0, 2)
                      + ":"
                      + time_format.slice(2, 4)

                    tabinput.setAttribute("value", time_format);
                  }
                  else {
                    if (content_fields[i][content_fields_head[1]] == "user_edit_id") {
                      tabinput.setAttribute("value", top.document.getElementById("user_username_content").innerHTML);
                      tabinput.setAttribute("title", top.document.getElementById("user_username_content").title);
                    }
                    else {
                      tabinput.setAttribute("value",
                      recordtoedit[0][content_fields[i][content_fields_head[1]]]);
                    }
                  }
                }
            }

            tabinput.setAttribute("disabled", "disabled");

            // if field is empty
            // if (
            //   (recordtoedit[0][content_fields[i][content_fields_head[1]]] == "")
            //   ||
            //   (recordtoedit[0][content_fields[i][content_fields_head[1]]] === undefined)
            // ) {
            //   ymp_tr.setAttribute("style", "display: none;");
            // }

          }
          // if no record in edit mode
          else {
            // enter default values
            if (content_fields[i][content_fields_head[2]].slice(-2) == "ID") {
              var d_id =
              new Date().getFullYear()
              + ("0" + (parseInt(new Date().getMonth()) + 1)).slice(-2)
              + ("0" + (new Date().getDate())).slice(-2)
              + ("0" + (new Date().getHours())).slice(-2)
              + ("0" + (new Date().getMinutes())).slice(-2)
              + ("0" + (new Date().getSeconds())).slice(-2);

              tabinput.setAttribute("value", d_id + content_fields[i][content_fields_head[4]]);
            }
            else {
              if (content_fields[i][content_fields_head[2]] == "Record Date") {
                var d_date =
                  new Date().getFullYear()
                  + ("0" + (parseInt(new Date().getMonth()) + 1)).slice(-2)
                  + ("0" + (new Date().getDate())).slice(-2);

                tabinput.setAttribute("value", d_date);
              }
              else {
                if (content_fields[i][content_fields_head[2]] == "Record Time") {
                  var d_time =
                    ("0" + (new Date().getHours())).slice(-2)
                    + ("0" + (new Date().getMinutes())).slice(-2)
                    + ("0" + (new Date().getSeconds())).slice(-2);

                  tabinput.setAttribute("value", d_time);
                }
                else {
                  if (content_fields[i][content_fields_head[2]] == "User Logged") {
                    tabinput.setAttribute("value", top.document.getElementById("user_username_content").innerHTML);
                    tabinput.setAttribute("title", top.document.getElementById("user_username_content").title);
                  }
                  else {
                    if (content_fields[i][content_fields_head[2]] == "Record Active Status") {
                      tabinput.setAttribute("value", true);
                    }
                    else {
                      if (content_fields[i][content_fields_head[3]] == "number") {
                        tabinput.setAttribute("value", 0);
                      }
                      else {
                        // if (content_fields[i][content_fields_head[3]] == "datetime-local") {
                        //   var default_date =
                        //   new Date().getFullYear()
                        //   + "-"
                        //   + ("0" + (parseInt(new Date().getMonth()) + 1)).slice(-2)
                        //   + "-"
                        //   + ("0" + (new Date().getDate())).slice(-2)
                        //   + "T"
                        //   + ("0" + (new Date().getHours())).slice(-2)
                        //   + ":"
                        //   + ("0" + (new Date().getMinutes())).slice(-2)
                        //   + ":"
                        //   + ("0" + (new Date().getSeconds())).slice(-2);
                        //
                        //   tabinput.setAttribute("value", default_date);
                        // }
                        // else {
                          // if (content_fields[i][content_fields_head[3]] == "date") {
                          //   var default_date =
                          //   new Date().getFullYear()
                          //   + "-"
                          //   + ("0" + (parseInt(new Date().getMonth()) + 1)).slice(-2)
                          //   + "-"
                          //   + ("0" + (new Date().getDate())).slice(-2)
                          //
                          //   tabinput.setAttribute("value", default_date);
                          // }
                          // else {
                          //   if (content_fields[i][content_fields_head[3]] == "time") {
                          //     var d_time =
                          //       ("0" + (new Date().getHours())).slice(-2)
                          //       + ":"
                          //       + ("0" + (new Date().getMinutes())).slice(-2)
                          //
                          //     tabinput.setAttribute("value", d_time);
                          //   }
                          //   else {
                              tabinput.setAttribute("value", "");
                          //   }
                          // }
                        // }
                      }
                    }
                  }
                }
              }
            }
            // /enter default values

            if (content_fields[i][content_fields_head[7]] == true) {
              tabinput.setAttribute("disabled", "disabled");
            }
          }

          tabinput.setAttribute("id", content_fields[i][content_fields_head[1]]);
          tabinput.setAttribute("type", content_fields[i][content_fields_head[3]]);

          ymp_td.appendChild(tabinput);
        }
        // /else last all
      }
      // /else if field input is not picklist then
    }
    // if field is static then
    else {
      // id row so that we can hide and unhide accordingly
      var ymp_tr = document.getElementById("tr_" + content_fields[i][content_fields_head[1]]);

      // if field picklist is present then
      if (content_fields[i][content_fields_head[6]] == "picklist") {
        JSONfile = top.getJSONfile("", content_fields[i][content_fields_head[9]]);

        var picklist_records = JSONfile[2];
        var picklist_records_head = JSONfile[3];

        // if field type is boolean then
        if (content_fields[i][content_fields_head[3]] == "boolean") {
          var tabinput = document.getElementById(content_fields[i][content_fields_head[1]]);
          tabinput.innerHTML = "";
          tabinput.disabled = false;

          for (var j = 0; j < picklist_records.length; j++) {

            var optionj = document.createElement("option");
            optionj.setAttribute("value", picklist_records[j][picklist_records_head[1]]);

            // if record in edit mode
            if (file_id != "") {
              if (recordtoedit[0][content_fields[i][content_fields_head[1]]] ==
                picklist_records[j][picklist_records_head[1]]) {
                  optionj.setAttribute("selected", "true");
              }
              tabinput.setAttribute("disabled", "disabled");
            }

            var optionjtext = document.createTextNode(picklist_records[j][picklist_records_head[2]]);
            optionj.appendChild(optionjtext);
            tabinput.appendChild(optionj);
          }
        }
        else {

          // if field type is text then
          if (content_fields[i][content_fields_head[3]] == "text") {

            var tabinput = document.getElementById(content_fields[i][content_fields_head[1]]);
            tabinput.innerHTML = "";

            for (var j = 0; j < picklist_records.length; j++) {

              var optionj = document.createElement("option");
              optionj.setAttribute("value", picklist_records[j][picklist_records_head[0]]);

              // if record in edit mode
              if (file_id != "") {

                if (recordtoedit[0][content_fields[i][content_fields_head[1]]] ==
                  picklist_records[j][picklist_records_head[0]]) {
                    optionj.setAttribute("selected", "true");
                  }
                  tabinput.setAttribute("disabled", "disabled");

                  // if empty record then hide row in view mode
                  // if (
                  //   (recordtoedit[0][content_fields[i][content_fields_head[1]]] == 1)
                  //   ||
                  //   (recordtoedit[0][content_fields[i][content_fields_head[1]]] === undefined)
                  // ) {
                  //   var ymp_tr =
                  //     document.getElementById("tr_" + content_fields[i][content_fields_head[1]]);
                  //   ymp_tr.setAttribute("style", "display: none;");
                  // }
                }

                var optionjtext = document.createTextNode(picklist_records[j][picklist_records_head[1]]);
                optionj.appendChild(optionjtext);
                tabinput.appendChild(optionj);
              }

              var ymp_link =
              document.getElementById(content_fields[i][content_fields_head[1]] + "_link");

              // for exceptional picklists that use files from other main pages

              // if picklist is part of user then
              if (content_fields[i][content_fields_head[9]].search("user") != -1) {

                // if picklist is part of job then
                if (content_fields[0][content_fields_head[1]].search("job") != -1) {

                  ymp_link.setAttribute("onclick", "get_details("
                  + "'main_picklist',"
                  + "'./" + content_fields[i][content_fields_head[9]]
                  + "','job_exception');");

                }
                // if picklist is not part of job then assign default onclick
                else {

                  ymp_link.setAttribute("onclick", "get_details("
                  + "'main_picklist',"
                  + "'./" + content_fields[i][content_fields_head[9]]
                  + "','');");

                }

              }
              // if picklist is not part of user then assign default onclick
              else {

                ymp_link.setAttribute("onclick", "get_details("
                + "'main_picklist',"
                + "'./" + content_fields[i][content_fields_head[9]]
                + "','');");

              }

          }
          else {

            // if pop-up is required then
            if (content_fields[i][content_fields_head[3]] == "array") {

              // the pop-up

              var ymp_link = document.getElementById(
                "link_" + content_fields[i][content_fields_head[1]]
              );
              var ymp_input = document.getElementById(
                "input_" + content_fields[i][content_fields_head[1]]
              );
              var ymp_output = document.getElementById(
                content_fields[i][content_fields_head[1]]
              );

              // if record in edit mode
              if (file_id != "") {

                // if data is not empty then
                if (recordtoedit[0][content_fields[i][content_fields_head[1]]] != "") {

                  top.refreshJSONrecord("tys_production/ympdb/popup/"
                    + content_fields[i][content_fields_head[1]] +
                    ".json", "[]");

                  // onclick is dependant on the view or edit modal position
                  ymp_link.setAttribute("onclick",
                    "createWindow('"
                    + content_fields[i][content_fields_head[1]]
                    + "', '"
                    + file_id
                    + "','view');"
                    + "document.getElementById('modal_form').addEventListener("
                    + "'click', handleinventoryclick);"
                    + "field_name = '"
                    + content_fields[i][content_fields_head[1]]
                    + "';"
                  );

                  // clear the inventory notification message
                  ymp_output.innerHTML = ""

                }
                // if record not in edit mode
                else {

                  top.refreshJSONrecord("tys_production/ympdb/popup/"
                    + content_fields[i][content_fields_head[1]] +
                    ".json", "[]");

                  // onclick is dependant on the view or edit modal position
                  ymp_link.setAttribute("onclick",
                    "alert('Inventory list is empty!');"
                  );

                  // clear the inventory notification message
                  ymp_output.innerHTML = ""

                }

              }
              // if no edit mode then
              else {

                top.refreshJSONrecord("tys_production/ympdb/popup/"
                  + content_fields[i][content_fields_head[1]] +
                  ".json", "[]");

                ymp_link.setAttribute("onclick",
                  "createWindow('"
                  + content_fields[i][content_fields_head[1]]
                  + "', '', '');"
                  + "document.getElementById('"
                  + content_fields[i][content_fields_head[1]]
                  + "').innerHTML = 'updating...';"
                  + "document.getElementById('modal_form').addEventListener("
                  + "'click', handleinventoryclick);"
                  + "document.getElementById('modal_form').addEventListener("
                  + "'mouseover', handleinventorymouse);"
                  + "field_name = '"
                  + content_fields[i][content_fields_head[1]]
                  + "';"
                );
                //listener **mouseover** works!!
                //listener **change** works!!
                //listener **click** works!!

              }
            }
          }
        }
      }
      // else if field input is not picklist then file details
      else {
        if (content_fields[i][content_fields_head[6]] == "file") {
          // if (content_fields[i][content_fields_head[5]] != "details_xtra") {

            // get the divider
            var ymp_div = document.getElementById("div_" + content_fields[i][content_fields_head[1]]);

            // get the link
            var ymp_link = document.getElementById("link_" + content_fields[i][content_fields_head[1]]);

            if (content_fields[i][content_fields_head[1]] == "job_photos") {

              top.refreshJSONrecord("tys_production/ympdb/popup/"
                + content_fields[i][content_fields_head[1]] +
                ".json", "[]");

              ymp_link.setAttribute("onclick",
                "createWindow('job_photos', '', 'add');"
                + "document.getElementById('modal_form').addEventListener("
                + "'click', handlephotosclick);"
                + "document.getElementById('modal_form').addEventListener("
                + "'mouseover', handlephotosmouse);"
              );
            }
            else {
              ymp_link.setAttribute("onclick",
                "document.getElementById('input_" + content_fields[i][content_fields_head[1]] + "').click();");
            }

            // get the options
            var ymp_options = document.getElementById("options_"
              + content_fields[i][content_fields_head[1]] + "_btn");

            // get the input box
            var ymp_input = document.getElementById("input_" + content_fields[i][content_fields_head[1]]);
            ymp_input.addEventListener("change", handleObjectSelect, false);

            // get the output box
            var ymp_output = document.getElementById(content_fields[i][content_fields_head[1]]);

            // if record in edit mode
            if (file_id != "") {
              var ymp_img = document.getElementById("img_" + content_fields[i][content_fields_head[1]]);

              // if it is not part of the pop-up details then
              if (content_fields[i][content_fields_head[5]] != "details_xtra") {
                ymp_options.setAttribute("style", "display: none;");

                // if field is not empty or not undefined
                if (
                  (recordtoedit[0][content_fields[i][content_fields_head[1]]] != "")
                  &&
                  (recordtoedit[0][content_fields[i][content_fields_head[1]]] !== undefined)
                ) {
                  ymp_output.innerHTML = ""
                  ymp_output.alt = recordtoedit[0][content_fields[i][content_fields_head[1]]];

                  ymp_img.setAttribute("src",
                    autochooseicon(recordtoedit[0][content_fields[i][content_fields_head[1]]]));

                  ymp_img.setAttribute("onclick", "opnfile('" + location.href.slice(8, 11)
                    + recordtoedit[0][content_fields[i][content_fields_head[1]]]
                    + "');");
                }
                // if field is empty
                // else {
                //   ymp_tr.setAttribute("style", "display: none;");
                // }

                ymp_img.setAttribute("style",
                  "cursor: pointer; margin-top: 6px; max-height: 50px; max-width: 75px;");

                ymp_div.appendChild(ymp_img);
              }
              // if it is part of the pop-up details then
              else {

                if (content_fields[i][content_fields_head[1]] == "job_photos") {

                  // if no photo then
                  if (recordtoedit[0][content_fields[i][content_fields_head[1]]] == "") {
                    ymp_link.setAttribute("onclick", "alert('No Photo uploaded!');");
                  }
                  else {

                    var photos_content = recordtoedit[0].job_photos;

                    top.refreshJSONrecord("tys_production/ympdb/popup/job_photos.json",
                      photos_content);

                    var count_photos = 0;
                    var count_12345 = 0;

                    for (var id_y in photos_content) {
                      if (photos_content != "[]") {
                        // increment count
                        count_photos = count_photos + 1;
                      }
                    }

                    var ymp_img1 = document.getElementById("img_job_technical_photo1");
                    ymp_img1.setAttribute("style", "display: none;");
                    var ymp_output1 = document.getElementById("job_technical_photo1");
                    ymp_output1.innerHTML = "(empty)";
                    var ymp_img2 = document.getElementById("img_job_technical_photo2");
                    ymp_img2.setAttribute("style", "display: none;");
                    var ymp_output2 = document.getElementById("job_technical_photo2");
                    ymp_output2.innerHTML = "(empty)";
                    var ymp_img3 = document.getElementById("img_job_technical_photo3");
                    ymp_img3.setAttribute("style", "display: none;");
                    var ymp_output3 = document.getElementById("job_technical_photo3");
                    ymp_output3.innerHTML = "(empty)";
                    var ymp_img4 = document.getElementById("img_job_technical_photo4");
                    ymp_img4.setAttribute("style", "display: none;");
                    var ymp_output4 = document.getElementById("job_technical_photo4");
                    ymp_output4.innerHTML = "(empty)";
                    var ymp_img5 = document.getElementById("img_job_technical_photo5");
                    ymp_img5.setAttribute("style", "display: none;");
                    var ymp_output5 = document.getElementById("job_technical_photo5");
                    ymp_output5.innerHTML = "(empty)";

                    // for the total number of photos from last to first
                    for (var j = count_photos; j > 0; j--) {

                      // increment count
                      count_12345 = count_12345 + 1;

                      // case for the last 5 photos selected
                      switch (count_12345) {
                        case 1:
                          ymp_output1.innerHTML = "";
                          ymp_img1.setAttribute("src", autochooseicon(photos_content["photo" + j]));
                          ymp_img1.setAttribute("style",
                            "cursor: pointer; display: inline; margin-top: 6px; max-height: 50px; max-width: 75px;");
                          break;
                        case 2:
                          ymp_output2.innerHTML = "";
                          ymp_img2.setAttribute("src", autochooseicon(photos_content["photo" + j]));
                          ymp_img2.setAttribute("style",
                            "cursor: pointer; display: inline; margin-top: 6px; max-height: 50px; max-width: 75px;");
                          break;
                        case 3:
                          ymp_output3.innerHTML = "";
                          ymp_img3.setAttribute("src", autochooseicon(photos_content["photo" + j]));
                          ymp_img3.setAttribute("style",
                            "cursor: pointer; display: inline; margin-top: 6px; max-height: 50px; max-width: 75px;");
                          break;
                        case 4:
                          ymp_output4.innerHTML = "";
                          ymp_img4.setAttribute("src", autochooseicon(photos_content["photo" + j]));
                          ymp_img4.setAttribute("style",
                            "cursor: pointer; display: inline; margin-top: 6px; max-height: 50px; max-width: 75px;");
                          break;
                        case 5:
                          ymp_output5.innerHTML = "";
                          ymp_img5.setAttribute("src", autochooseicon(photos_content["photo" + j]));
                          ymp_img5.setAttribute("style",
                            "cursor: pointer; display: inline; margin-top: 6px; max-height: 50px; max-width: 75px;");
                          break;
                      }

                    }

                    ymp_link.setAttribute("onclick",
                      "createWindow('job_photos', '"  + recordtoedit[0].job_id + "','view');"
                      + "document.getElementById('modal_form').addEventListener("
                      + "'click', handlephotosclick);"
                    );

                  }

                }
                else {

                  // if no file then
                  if (recordtoedit[0][content_fields[i][content_fields_head[1]]] == "") {
                    ymp_link.setAttribute("onclick", "alert('No file to Open!');");
                  }
                  else {
                    ymp_link.setAttribute("onclick", "opnfile('" + location.href.slice(8, 11) +
                    recordtoedit[0][content_fields[i][content_fields_head[1]]]
                    + "');");
                  }

                  // clear output
                  ymp_output.innerHTML = ""

                }

              }
          }
        }
        // if field input is not file then
        else {

          var tabinput = document.getElementById(content_fields[i][content_fields_head[1]]);

          // if record in edit mode
          if (file_id != "") {

            if (content_fields[i][content_fields_head[3]] == "datetime-local") {
              var datetime_format =
              recordtoedit[0][content_fields[i][content_fields_head[1]]].toString();

              if ((datetime_format.slice(12, 14) == "00") || (datetime_format.slice(12, 14) == "")) {
                datetime_format =
                datetime_format.slice(0, 4)
                + "-"
                + datetime_format.slice(4, 6)
                + "-"
                + datetime_format.slice(6, 8)
                + "T"
                + datetime_format.slice(8, 10)
                + ":"
                + datetime_format.slice(10, 12)
                + ":00"
              }
              else {
                datetime_format =
                datetime_format.slice(0, 4)
                + "-"
                + datetime_format.slice(4, 6)
                + "-"
                + datetime_format.slice(6, 8)
                + "T"
                + datetime_format.slice(8, 10)
                + ":"
                + datetime_format.slice(10, 12)
                + ":"
                + datetime_format.slice(12, 14);
              }

              tabinput.setAttribute("value", datetime_format);
            }
            else {
              if (content_fields[i][content_fields_head[3]] == "date") {
                var date_format =
                recordtoedit[0][content_fields[i][content_fields_head[1]]].toString();

                date_format =
                date_format.slice(0, 4)
                + "-"
                + date_format.slice(4, 6)
                + "-"
                + date_format.slice(6, 8);

                tabinput.setAttribute("value", date_format);
              }
              else {
                if (content_fields[i][content_fields_head[3]] == "time") {
                  var time_format =
                    recordtoedit[0][content_fields[i][content_fields_head[1]]].toString();

                  if (time_format.length == 3) {
                    time_format = "0" + time_format;
                  }

                  time_format =
                    time_format.slice(0, 2)
                    + ":"
                    + time_format.slice(2, 4)

                  tabinput.setAttribute("value", time_format);
                }
                else {
                  if (content_fields[i][content_fields_head[1]] == "user_edit_id") {
                    tabinput.setAttribute("value", top.document.getElementById("user_username_content").innerHTML);
                    tabinput.setAttribute("title", top.document.getElementById("user_username_content").title);
                  }
                  else {
                    tabinput.setAttribute("value",
                    recordtoedit[0][content_fields[i][content_fields_head[1]]]);
                    tabinput.innerHTML =
                    recordtoedit[0][content_fields[i][content_fields_head[1]]];
                  }
                }
              }
            }

            tabinput.setAttribute("disabled", "disabled");

            // if field is empty
            // if (
            //   (recordtoedit[0][content_fields[i][content_fields_head[1]]] == "")
            //   ||
            //   (recordtoedit[0][content_fields[i][content_fields_head[1]]] === undefined)
            // ) {
            //   var ymp_tr =
            //   document.getElementById("tr_" + content_fields[i][content_fields_head[1]]);
            //   ymp_tr.setAttribute("style", "display: none;");
            // }

          }
          // if no record in edit mode
          else {

            // enter default values
            if (content_fields[i][content_fields_head[2]].slice(-2) == "ID") {
              var d_id =
              new Date().getFullYear()
              + ("0" + (parseInt(new Date().getMonth()) + 1)).slice(-2)
              + ("0" + (new Date().getDate())).slice(-2)
              + ("0" + (new Date().getHours())).slice(-2)
              + ("0" + (new Date().getMinutes())).slice(-2)
              + ("0" + (new Date().getSeconds())).slice(-2);

              tabinput.setAttribute("value", d_id + content_fields[i][content_fields_head[4]]);
            }
            else {
              if (content_fields[i][content_fields_head[2]] == "Record Date") {
                var d_date =
                new Date().getFullYear()
                + ("0" + (parseInt(new Date().getMonth()) + 1)).slice(-2)
                + ("0" + (new Date().getDate())).slice(-2);

                tabinput.setAttribute("value", d_date);
              }
              else {
                if (content_fields[i][content_fields_head[2]] == "Record Time") {
                  var d_time =
                  ("0" + (new Date().getHours())).slice(-2)
                  + ("0" + (new Date().getMinutes())).slice(-2)
                  + ("0" + (new Date().getSeconds())).slice(-2);

                  tabinput.setAttribute("value", d_time);
                }
                else {
                  if (content_fields[i][content_fields_head[2]] == "Date From") {
                      var default_date =
                        new Date().getFullYear()
                        + "-"
                        + ("0" + (parseInt(new Date().getMonth()) + 1)).slice(-2)
                        + "-"
                        + ("0" + (new Date().getDate())).slice(-2)

                      tabinput.setAttribute("value", default_date);
                  }
                  else {
                    if (content_fields[i][content_fields_head[2]] == "Time Finished")
                    {
                      var d_time =
                        ("0" + (new Date().getHours())).slice(-2)
                        + ":"
                        + ("0" + (new Date().getMinutes())).slice(-2)

                      tabinput.setAttribute("value", d_time);
                    }
                    else {
                      if (content_fields[i][content_fields_head[2]] == "User Logged") {
                        tabinput.setAttribute("value", top.document.getElementById("user_username_content").innerHTML);
                        tabinput.setAttribute("title", top.document.getElementById("user_username_content").title);
                      }
                      else {
                        if (content_fields[i][content_fields_head[2]] == "Record Active Status") {
                          tabinput.setAttribute("value", true);
                        }
                        else {
                          if (content_fields[i][content_fields_head[3]] == "number") {
                            tabinput.setAttribute("value", 0);
                          }
                          else {
                            tabinput.setAttribute("value", "");
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            // /enter default values

            // if field is not set to disabled then
            if (content_fields[i][content_fields_head[7]] != true) {
              document.getElementById(content_fields[i][content_fields_head[1]]).disabled = false;
            }
          }
          // /edit mode or not
        }
      }
    }
    // /if field is static then

    if (content_fields[i]["field_view"] == "log") {
      ymp_tr.setAttribute("style", "display: none;")
    }
  }
  // /add JSON data to the table as rows - go through all the record fields

  // finally add the newly created table with JSON data to a container
  var divContainer1 = document.getElementById("show_records_input1");
  divContainer1.innerHTML = "";
  divContainer1.appendChild(ymp_table1);

  // finally add the newly created table with JSON data to a container
  var divContainer2 = document.getElementById("show_records_input2");
  divContainer2.innerHTML = "";
  divContainer2.appendChild(ymp_table2);

  // finally add the newly created table with JSON data to a container
  var divContainer3 = document.getElementById("show_records_input3");
  divContainer3.innerHTML = "";
  divContainer3.appendChild(ymp_table3);
}
// /populate_division

// Collect event for the Photo handler
function handleObjectSelect(evt) {
  var files = evt.target.files; // FileList object
  var file_path = files[0]["path"];

  // if event listener detected a change but still empty entry then
  var file_extension = evt.target.id;
  file_extension = file_extension.replace("input_", "");

  // assign url to the right field
  var ymp_output = document.getElementById(file_extension);
  ymp_output.innerHTML = "";
  ymp_output.alt = file_path;

  var ymp_img = document.getElementById("img_" + file_extension);
  var ymp_remove_img = document.getElementById("remove_" + file_extension);

  if (autochooseicon(file_path) == file_path) {
    ymp_img.setAttribute("src", file_path);
  }
  else {
    ymp_img.setAttribute("src", autochooseicon(file_path));
  }

  ymp_img.setAttribute("style",
    "cursor: pointer; margin-top: 6px; max-height: 50px; max-width: 75px;");
  ymp_remove_img.setAttribute("style", "display: inline;");
}
// /handleObjectSelect

// Return correct img icon
function autochooseicon(src_path) {
  switch (src_path.slice(-5).substring(src_path.slice(-5).indexOf(".")+1).toLowerCase()) {
    // for img - accepted formats
    case "jpg":
    case "png":
    case "gif":
      if (location.pathname.slice(1, 4) == src_path.slice(0, 3).replace("\\", "/")) {
        return src_path;
      }
      else {
        return location.pathname.slice(1, 4) + src_path;
      }
      break;
    case "pdf":
      return location.pathname.slice(1, location.pathname.search("app/"))
        + "app/img/fileicon_pdf.png";
      break;
    case "doc":
    case "docx":
      return location.pathname.slice(1, location.pathname.search("app/"))
        + "app/img/fileicon_doc.png";
      break;
    case "xls":
    case "xlsx":
      return location.pathname.slice(1, location.pathname.search("app/"))
        + "app/img/fileicon_xls.png";
      break;
    case "ppt":
    case "pptx":
    // return "app/img/ppt_icon.png";
    return location.pathname.slice(1, location.pathname.search("app/"))
      + "app/img/fileicon_ppt.png";
    break;
    case "mp4":
      return location.pathname.slice(1, location.pathname.search("app/"))
        + "app/img/fileicon_video.png";
      break;
    default:
      return location.pathname.slice(1, location.pathname.search("app/"))
        + "app/img/fileicon_any.png";
      break;
  }
}
// /autochooseicon

// Helps with opening up files in their default player
function opnfile(filetoopen) {
  const opn = top.require('opn');

  opn(filetoopen);
}
// /opnfile

// To clear chosen file
function removefile(filetoremove) {

  // assign url input value
  var ymp_input = document.getElementById("input_" + filetoremove);
  ymp_input.value = "";

  // assign url output value
  var ymp_output = document.getElementById(filetoremove);
  ymp_output.innerHTML = "(empty)";
  ymp_output.alt = "";

  var ymp_img = document.getElementById("img_" + filetoremove);
  ymp_img.setAttribute("style", "display: none;");
  var ymp_remove_img = document.getElementById("remove_" + filetoremove);
  ymp_remove_img.setAttribute("style", "display: none;");

}
// /removefile

// When the user clicks add new record
function addrecord() {
  var new_records = '"' + content_fields[0][content_fields_head[1]] + '"' + ':' +
    '"' + document.getElementById(content_fields[0][content_fields_head[1]]).value + '"';

  for (var j = 1; j < content_fields.length; j++) {
    if (content_fields[j][content_fields_head[3]] == "number") {
      new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
        Number(document.getElementById(content_fields[j][content_fields_head[1]]).value);
    }
    else {
      if (content_fields[j][content_fields_head[3]] == "boolean") {
        new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
          document.getElementById(content_fields[j][content_fields_head[1]]).value;
      }
      else {
        if (content_fields[j][content_fields_head[3]] == "array") {

          // get the data in the appropriate pop-up
          JSONfile = top.getJSONfile(
            "",
            "tys_production/ympdb/popup/" + content_fields[j][content_fields_head[1]] + ".json"
          );
          var popup_data = JSONfile[2];

          if (popup_data == "[]") {
            new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
              '""';
          }
          else {
            new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
              JSON.stringify(popup_data[0]);
          }

        }
        else {

          if (content_fields[j][content_fields_head[6]] == "file") {
            var url_file = document.getElementById(content_fields[j][content_fields_head[1]]).alt;
            var url_transfer = ""

          if (
            (url_file != "")
            &&
            (url_file != undefined)
          ) {
            url_transfer = document.getElementById("input_" + content_fields[j][content_fields_head[1]]).value;
            url_transfer = url_transfer.substring(url_transfer.indexOf("\\fakepath\\")+10);
            url_transfer = content_fields[j][content_fields_head[9]] + "/" + url_transfer;

            var fs = top.require('fs');

            fs.createReadStream(url_file).pipe(
              fs.createWriteStream(location.href.slice(8, 11) + url_transfer));

              url_transfer.replace("\\", "/");
            }

            new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
            '"' + url_transfer + '"';
          }
          else {
            if (
              (content_fields[j][content_fields_head[3]] == "date")
              ||
              (content_fields[j][content_fields_head[3]] == "datetime-local")
              ||
              (content_fields[j][content_fields_head[3]] == "time")
            ) {
              var collect_date = document.getElementById(content_fields[j][content_fields_head[1]]).value;

              collect_date = collect_date.replace(/-/g, "");
              collect_date = collect_date.replace("T", "");
              collect_date = collect_date.replace(/:/g, "");

              if (collect_date == "") {
                new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
                0;
              }
              else {
                new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
                Number(collect_date);
              }
            }
            else {
              if (content_fields[j][content_fields_head[1]] == "user_edit_id") {
                new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
                '"' + document.getElementById(content_fields[j][content_fields_head[1]]).title + '"';
              }
              else {
                new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
                '"' + document.getElementById(content_fields[j][content_fields_head[1]]).value + '"';
              }
            }
          }
        }
      }
    }
  }

  new_records = '{' + new_records + '}';
  new_records = JSON.parse(new_records);

  // add JSON data to the table as rows
  for (var i = 0; i < content_fields.length; i++) {
    if (content_fields[i][content_fields_head[5]] != "details_xtra") {
      document.getElementById(content_fields[i][content_fields_head[1]]).disabled = true;

      if (content_fields[i][content_fields_head[6]] == "file") {
        document.getElementById(
          "options_" + content_fields[i][content_fields_head[1]] + "_btn").style.display = "none";
      }
    }
    else {
      document.getElementById("link_" + content_fields[i][content_fields_head[1]]).onclick = "";
    }
  }

  top.addJSONrecord(edit_json_file, new_records);
}
// /addrecord

// Initiate the edit record function
function editrecord() {
  // add JSON data to the table as rows
  for (var i = 0; i < content_fields.length; i++) {

    // display all fields
    try{
      document.getElementById("tr_" +
      content_fields[i][content_fields_head[1]]).style.display = "run-in";
    }
    catch(err) {}

    // if field is a password then
    if (content_fields[i][content_fields_head[3]] == "password") {
      document.getElementById(content_fields[i][content_fields_head[1]]).type = "password";
    }

    // if picklist then
    if (content_fields[i][content_fields_head[6]] == "picklist") {
      if (content_fields[i][content_fields_head[3]] == "text") {
        document.getElementById(
          content_fields[i][content_fields_head[1]] + "_details").style.display = "inline";
      }
      else {
        if (content_fields[i][content_fields_head[3]] == "array") {

          // update the notification message with count_x

          var popup_content =
          recordtoedit[0][content_fields[i][content_fields_head[1]]];
          var count_x = 0;

          for (var id_y in popup_content) {
            // increment count
            count_x = count_x + 1;
          }
          document.getElementById(
            content_fields[i][content_fields_head[1]]).innerHTML = "x" + count_x + " items";
          document.getElementById(
            content_fields[i][content_fields_head[1]]).title = count_x;

          // /update the notification message with count_x

          var ymp_link = document.getElementById(
            "link_" + content_fields[i][content_fields_head[1]]
          );

          // if record in edit mode
          if (
            (recordtoedit[0][content_fields[i][content_fields_head[1]]] != "")
            &&
            (recordtoedit[0][content_fields[i][content_fields_head[1]]] !== undefined)
          ) {
              ymp_link.setAttribute("onclick",
              "createWindow('"
                + content_fields[i][content_fields_head[1]]
                + "', '"
                + recordtoedit[0][content_fields[0][content_fields_head[1]]]
                +"', '');"
                + "document.getElementById('"
                + content_fields[i][content_fields_head[1]]
                + "').innerHTML = 'updating...';"
                + "document.getElementById('modal_form').addEventListener("
                + "'click', handleinventoryclick);"
                + "document.getElementById('modal_form').addEventListener("
                + "'mouseover', handleinventorymouse);"
                + "field_name = '"
                + content_fields[i][content_fields_head[1]]
                + "';"
              );
            }
            // if record not in edit mode
            else {
              ymp_link.setAttribute("onclick",
              "createWindow('"
              + content_fields[i][content_fields_head[1]]
              + "', '', '');"
              + "document.getElementById('"
              + content_fields[i][content_fields_head[1]]
              + "').innerHTML = 'updating...';"
              + "document.getElementById('modal_form').addEventListener("
              + "'click', handleinventoryclick);"
              + "document.getElementById('modal_form').addEventListener("
              + "'mouseover', handleinventorymouse);"
              + "field_name = '"
              + content_fields[i][content_fields_head[1]]
              + "';"
            );
          }

        }
      }
    }

    // if file then
    if (content_fields[i][content_fields_head[6]] == "file") {
      var ymp_div = document.getElementById("div_" + content_fields[i][content_fields_head[1]]);
      // get the output box
      var ymp_output = document.getElementById(content_fields[i][content_fields_head[1]]);
      var ymp_remove_img =
        document.getElementById("remove_" + content_fields[i][content_fields_head[1]]);

      // if not details extra
      if (content_fields[i][content_fields_head[5]] != "details_xtra") {
        document.getElementById("options_" + content_fields[i][content_fields_head[1]] + "_btn").style =
          "display: inline; height: 15px; width: 15px;";

        if (ymp_output.innerHTML.search("(empty)") == -1) {
          ymp_remove_img.setAttribute("style", "display: inline;");
        }
      }
      // else if details extra
      else {
        // change the link to select a different file
        var ymp_link = document.getElementById("link_" + content_fields[i][content_fields_head[1]]);

        if (content_fields[i][content_fields_head[1]] == "job_photos") {

          ymp_link.setAttribute("onclick",
            "createWindow('job_photos', '" + recordtoedit[0].job_id + "','edit');"
            + "document.getElementById('modal_form').addEventListener("
            + "'click', handlephotosclick);"
            + "document.getElementById('modal_form').addEventListener("
            + "'mouseover', handlephotosmouse);"
          );

        }
        else {
          ymp_link.setAttribute("onclick",
            "document.getElementById('input_" + content_fields[i][content_fields_head[1]] + "').click();");

          // if record in edit mode
          if (
            (recordtoedit[0][content_fields[i][content_fields_head[1]]] != "")
            &&
            (recordtoedit[0][content_fields[i][content_fields_head[1]]] !== undefined)
          ) {
            ymp_output.innerHTML = ""
            ymp_output.alt = recordtoedit[0][content_fields[i][content_fields_head[1]]];

            var ymp_img = document.getElementById("img_" + content_fields[i][content_fields_head[1]]);
            ymp_img.setAttribute("src",
              autochooseicon(recordtoedit[0][content_fields[i][content_fields_head[1]]]));
            ymp_img.setAttribute("style",
              "cursor: pointer; margin-top: 6px; max-height: 50px; max-width: 75px;");

            ymp_remove_img.setAttribute("style", "display: inline;");
          }
          // if record not in edit mode
          else {
            ymp_output.innerHTML = "(empty)"
          }
          // else {
          //   ymp_tr.setAttribute("style", "display: none;");
          // }
        }
      }
    }

    // if field is not set to disabled then
    if (content_fields[i][content_fields_head[7]] != true) {
      if (content_fields[i][content_fields_head[5]] != "details_xtra") {
        document.getElementById(content_fields[i][content_fields_head[1]]).disabled = false;
      }
    }
  }
}
// /editrecord

// Update the edited record function
function updaterecord() {
  var new_records = '"' + content_fields[0][content_fields_head[1]] + '"' + ':' +
    '"' + document.getElementById(content_fields[0][content_fields_head[1]]).value + '"';

  for (var j = 1; j < content_fields.length; j++) {
    if (content_fields[j][content_fields_head[3]] == "number") {
      new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
        Number(document.getElementById(content_fields[j][content_fields_head[1]]).value);
    }
    else {
      if (content_fields[j][content_fields_head[3]] == "boolean") {
        new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
          document.getElementById(content_fields[j][content_fields_head[1]]).value;
      }
      else {
        if (content_fields[j][content_fields_head[3]] == "array") {

          // get the data in the appropriate pop-up
          JSONfile = top.getJSONfile(
            "",
            "tys_production/ympdb/popup/" + content_fields[j][content_fields_head[1]] + ".json"
          );
          var popup_data = JSONfile[2];

          if (popup_data == "[]") {
            new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
              '""';
          }
          else {
            new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
              JSON.stringify(popup_data[0]);
          }

        }
        else {
          if (content_fields[j][content_fields_head[6]] == "file") {
            var url_file = document.getElementById(content_fields[j][content_fields_head[1]]).alt;
            var url_transfer = ""

            if (document.getElementById("input_"
              + content_fields[j][content_fields_head[1]]).value != "")
            {
              if ((url_file != "") || (url_file != undefined)) {

                url_transfer = document.getElementById("input_" + content_fields[j][content_fields_head[1]]).value;
                url_transfer = url_transfer.substring(url_transfer.indexOf("\\fakepath\\")+10);
                url_transfer = content_fields[j][content_fields_head[9]] + "/" + url_transfer;

                var fs = top.require('fs');

                fs.createReadStream(url_file).pipe(
                  fs.createWriteStream(location.href.slice(8, 11) + url_transfer));

                url_transfer.replace("\\", "/");
              }
            }
            else {
              if (document.getElementById(content_fields[j][content_fields_head[1]]).alt != null) {
                url_transfer = document.getElementById(content_fields[j][content_fields_head[1]]).alt;
              }
            }

            new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
            '"' + url_transfer + '"';
          }
          else {
            if (
              (content_fields[j][content_fields_head[3]] == "date")
              ||
              (content_fields[j][content_fields_head[3]] == "datetime-local")
              ||
              (content_fields[j][content_fields_head[3]] == "time")
            ) {
              var collect_date = document.getElementById(content_fields[j][content_fields_head[1]]).value;

              collect_date = collect_date.replace(/-/g, "");
              collect_date = collect_date.replace("T", "");
              collect_date = collect_date.replace(/:/g, "");

              if (collect_date == "") {
                new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
                0;
              }
              else {
                new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
                Number(collect_date);
              }
            }
            else {
              if (content_fields[j][content_fields_head[1]] == "user_edit_id") {
                new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
                '"' + document.getElementById(content_fields[j][content_fields_head[1]]).title + '"';
              }
              else {
                new_records = new_records + ", " + '"' + content_fields[j][content_fields_head[1]] + '"' + ":" +
                '"' + document.getElementById(content_fields[j][content_fields_head[1]]).value + '"';
              }
            }
          }
        }
      }
    }
  }

  new_records = '{' + new_records + '}';
  new_records = JSON.parse(new_records);

  // add JSON data to the table as rows
  for (var i = 0; i < content_fields.length; i++) {
    if (content_fields[i][content_fields_head[5]] != "details_xtra") {
      document.getElementById(content_fields[i][content_fields_head[1]]).disabled = true;
    }
  }

  top.updateJSONfile(edit_json_file, recordtoedit[0][content_fields[0][content_fields_head[1]]], new_records);
}
// /updaterecord

// Delete record function
function confirmdeleterecord() {
  top.deleteJSONrecord(edit_json_file, recordtoedit[0][content_fields[0][content_fields_head[1]]]);
}
// /deleterecord
