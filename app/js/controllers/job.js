// Collect all parameters passed via address
var parameters = location.search.substring(1).split("&");
var pg_key1 = parameters[0];
var pg_key2 = parameters[1];
var pg_key3 = parameters[2];

var picklist_records = [];
var picklist_records_head = [];

var count_x = 0;
var click_inventory = 0;
var click_photos = 0;

// Manage all requests coming to pop_up
function case_popup() {
  switch (pg_key1) {
    case "job_inventory":
      inventory_click();
      break;
    case "job_photos":
      photos_click();
      break;
  }
}
// /case_popup

// Inventory click action
function inventory_click() {

  // get all the related data from JSON Files re Job
  var JSONfile = getJSONfile(
    "tys_production/ympdb/field/field_job.json",
    "tys_production/ympdb/job.json");
  var content_fields = JSONfile[0];
  var content_fields_head = JSONfile[1];
  var content_records = JSONfile[2];
  var content_records_head = JSONfile[3];

  var inventory_title = "";

  for (var i = 0; i < content_fields.length; i++) {
    if (content_fields[i].field == "job_inventory") {
      inventory_title = content_fields[i].field_title;
      inventory_url = content_fields[i].field_url;
    }
  }

  // build Pop-Up content

  var minion_h3 = document.createElement("h3"); // header3
  minion_h3.innerHTML = inventory_title;

  // get all the related data from JSON Files re Inventory picklist
  JSONfile = getJSONfile("", inventory_url);
  picklist_records = JSONfile[2];
  picklist_records_head = JSONfile[3];

  var minion_table = document.createElement("table"); // table
  // set table font color white
  minion_table.setAttribute("style", "color: #fff;");
  minion_table.setAttribute("id", "table_inventory");

  JSONfile = top.getJSONfile("", "tys_production/ympdb/popup/job_inventory.json");
  var inventory_content = JSONfile[2];

  // if record in edit mode or some items have already been listed then
  if ((pg_key2 != "") || (inventory_content[0] != "[]")) {

    if (pg_key3 == "view") {
      if (pg_key2 != "") {
        // get the related record to edit
        var recordtoedit = getObjects(content_records, content_records_head[0], pg_key2);

        inventory_content = recordtoedit[0].job_inventory;
        refreshJSONrecord("tys_production/ympdb/popup/job_inventory.json", inventory_content);
        inventory_content = JSON.parse("[" + JSON.stringify(inventory_content) + "]");

      }
    }

    for (var id_y in inventory_content[0]) {

      // increment count
      count_x = count_x + 1;

      var minion_tr = minion_table.insertRow(-1); // row
      var minion_td = minion_tr.insertCell(-1); // cell 1
      minion_td.innerHTML = "Inventory Part " + count_x;

      var minion_td = minion_tr.insertCell(-1); // cell 2
      var tabinput = document.createElement("select"); // table select drop-down
      tabinput.setAttribute("id", "inventory_part_" + count_x)
      tabinput.setAttribute("class", "default_txtbox");

      for (var j = 0; j < picklist_records.length; j++) {

        var optionj = document.createElement("option"); // table select drop-down option
        optionj.setAttribute("value", picklist_records[j][picklist_records_head[0]]);

        if (inventory_content[0]["inventory_id" + count_x] ==
          picklist_records[j][picklist_records_head[0]]) {
            optionj.setAttribute("selected", "true");
        }
        if (pg_key3 != "") {
          tabinput.setAttribute("disabled", "disabled");
        }

        var optionjtext = document.createTextNode(picklist_records[j][picklist_records_head[1]]);
        optionj.appendChild(optionjtext);
        tabinput.appendChild(optionj);

        minion_td.appendChild(tabinput);
      }

    }
  }
  // if record not in edit mode
  else {

    // increment count
    count_x = count_x + 1;

    var minion_tr = minion_table.insertRow(-1); // row

    var minion_td = minion_tr.insertCell(-1); // cell 1
    minion_td.innerHTML = "Inventory Part " + count_x;

    var minion_td = minion_tr.insertCell(-1); // cell 2
    var tabinput = document.createElement("select"); // table select drop-down
    tabinput.setAttribute("id", "inventory_part_" + count_x)
    tabinput.setAttribute("class", "default_txtbox");

    for (var j = 0; j < picklist_records.length; j++) {

      var optionj = document.createElement("option"); // table select drop-down option
      optionj.setAttribute("value", picklist_records[j][picklist_records_head[0]]);

      var optionjtext = document.createTextNode(picklist_records[j][picklist_records_head[1]]);
      optionj.appendChild(optionjtext);
      tabinput.appendChild(optionj);

      minion_td.appendChild(tabinput);
    }
  }

  if (pg_key3 == "") {
    // add new field button
    var minion_td = minion_tr.insertCell(-1); // cell 3

    var minion_link = document.createElement("a");  // link
    minion_link.setAttribute("href", "#");
    minion_link.setAttribute("onclick", "add_newinventory();");
    minion_link.setAttribute("id", "link_inventory_part_" + count_x);

    var add_inventory = document.createElement("img");
    add_inventory.setAttribute("src", "../../img/job_addbtn.png");
    add_inventory.setAttribute("title", "Add Inventory");
    add_inventory.setAttribute("id", "img_addinventory_part_" + count_x);

    minion_link.appendChild(add_inventory);
    minion_td.appendChild(minion_link);
    // /add new field button

    // add save button
    var btn_save = document.createElement("button");
    var btn_save_text = document.createTextNode("Save");
    btn_save.setAttribute("class", "greenbtnmodal");
    btn_save.setAttribute("onclick", "save_inventorylist();");
    btn_save.appendChild(btn_save_text);
    // /add save button
  }

  // add cancel button
  var btn_cancel = document.createElement("button");
  var btn_cancel_text = document.createTextNode("Cancel");
  btn_cancel.setAttribute("class", "redbtnmodal");
  btn_cancel.setAttribute("onclick", "window.close();");
  btn_cancel.appendChild(btn_cancel_text);
  // /add cancel button

  var minion_div = document.getElementById("div_popup");
  minion_div.innerHTML = "";
  minion_div.appendChild(minion_h3);
  minion_div.appendChild(minion_table);
  if (pg_key3 == "") {
    minion_div.appendChild(btn_save);
  }
  minion_div.appendChild(btn_cancel);

  minion_div.setAttribute("style", "margin-left: 10px;");

}
// /inventory_click

// Add new inventory select box
function add_newinventory() {

  var remove_inventory = document.getElementById("img_addinventory_part_" + count_x);
  remove_inventory.parentNode.removeChild(remove_inventory);

  // increment count
  count_x = count_x + 1;

  var minion_table = document.getElementById("table_inventory");

  var minion_tr = minion_table.insertRow(-1); // row
  var minion_td = minion_tr.insertCell(-1); // cell 1
  minion_td.innerHTML = "Inventory Part " + count_x;

  var minion_td = minion_tr.insertCell(-1); // cell 2
  var tabinput = document.createElement("select"); // table select drop-down
  tabinput.setAttribute("id", "inventory_part_" + count_x)
  tabinput.setAttribute("class", "default_txtbox");

  for (var j = 0; j < picklist_records.length; j++) {

    var optionj = document.createElement("option"); // table select drop-down option
    optionj.setAttribute("value", picklist_records[j][picklist_records_head[0]]);

    var optionjtext = document.createTextNode(picklist_records[j][picklist_records_head[1]]);
    optionj.appendChild(optionjtext);
    tabinput.appendChild(optionj);

    minion_td.appendChild(tabinput);
  }

  var minion_td = minion_tr.insertCell(-1); // cell 3

  var minion_link = document.createElement("a");  // link
  minion_link.setAttribute("href", "#");
  minion_link.setAttribute("onclick", "add_newinventory();");
  minion_link.setAttribute("id", "link_inventory_part_" + count_x);

  var add_inventory = document.createElement("img");
  add_inventory.setAttribute("src", "../../img/job_addbtn.png");
  add_inventory.setAttribute("title", "Add Inventory");
  add_inventory.setAttribute("id", "img_addinventory_part_" + count_x);

  minion_link.appendChild(add_inventory);
  minion_td.appendChild(minion_link);

}
// /add_newinventory

// Saves an inventory list for a job to be entered
function save_inventorylist() {

  var new_records = "[]";
  var valid_counts = 0;

  for (var i = 1; i < count_x + 1; i++) {
    if (document.getElementById("inventory_part_" + i).selectedIndex != 0) {
      // increment valid_counts
      valid_counts = valid_counts + 1;

      if (new_records == "[]") {
        new_records = '"inventory_id' + valid_counts + '":"' +
          document.getElementById("inventory_part_" + i).value + '"';
      }
      else {
        new_records = new_records + ',"inventory_id' + valid_counts + '":"' +
          document.getElementById("inventory_part_" + i).value + '"';
      }
    }
  }

  if (new_records != "[]") {
    new_records = '{' + new_records + '}';
    new_records = JSON.parse(new_records);
  }

  document.getElementById("modal_load").style.display="block";

  refreshJSONrecord("tys_production/ympdb/popup/job_inventory.json", new_records);

  setTimeout(
    function(){

      window.close();

    }, 2000);

}
// /save_inventorylist

// Specifically used to handle the count of items in the popup list
function handleinventoryclick() {

  // console.log("click works!");

  // increment click_inventory
  click_inventory = click_inventory + 1;

  // continue with listener until 2nd click
    // 1st click open pop-up
    // 2nd click away from pop-up and stop listener
  if (click_inventory > 1) {

    var JSONfile = top.getJSONfile("", "tys_production/ympdb/popup/" +
      field_name + ".json");

    popup_content = JSONfile[2];
    var count_x = 0;

    for (var id_y in popup_content[0]) {
      if (popup_content[0] != "[]") {
        // increment count
        count_x = count_x + 1;
      }
    }

    if (document.getElementById(field_name).innerHTML != "") {
      document.getElementById(field_name).innerHTML = "x" + count_x + " items";
    }
    document.getElementById(field_name).title = count_x;

    document.getElementById('modal_form').removeEventListener(
      'click', handleinventoryclick);
    document.getElementById('modal_form').removeEventListener(
      'mouseover', handleinventorymouse);

    // reset click_inventory
    click_inventory = 0;

    try{win.close();}catch(err){}

  }

}
// /handleinventoryclick

// Specifically used to handle the count of items in the popup list
function handleinventorymouse() {

  // console.log("mouse works!");

  var JSONfile = top.getJSONfile("", "tys_production/ympdb/popup/" +
    field_name + ".json");

  var popup_content = JSONfile[2];
  var count_x = 0;

  for (var id_y in popup_content[0]) {
    if (popup_content[0] != "[]") {
      // increment count
      count_x = count_x + 1;
    }
  }

  document.getElementById(field_name).innerHTML = "x" + count_x + " items";

  if (document.getElementById(field_name).title != count_x) {

    document.getElementById(field_name).innerHTML = "x" + count_x + " items";
    document.getElementById(field_name).title = count_x;

    document.getElementById('modal_form').removeEventListener(
      'click', handleinventoryclick);
    document.getElementById('modal_form').removeEventListener(
      'mouseover', handleinventorymouse);

  }

}
// /handleinventorymouse

// Single history click action
function single_click() {
  document.getElementById("div_singlehistory").style.display = "inline";
  document.getElementById("div_singlehistory").style.float = "left";
  document.getElementById("div_grouphistory").style.display = "none";
}
// /single_click

// Group history click action
function group_click(group_id) {
  document.getElementById("div_singlehistory").style.display = "none";
  document.getElementById("div_grouphistory").style.display = "inline";
  document.getElementById("div_grouphistory").style.float = "left";

  if (group_id == "1") {

    // update Group History
    var ymp_minidiv1 = document.getElementById("job_historyreport1");
    ymp_minidiv1.innerHTML = "No Group selected!";
    var ymp_minidiv2 = document.getElementById("job_historyreport2");
    ymp_minidiv2.innerHTML = "";

  }
  else {

    // create dynamic table
    var ymp_minitoptable = document.createElement("table"); // table
    // border light grey and font yelllow
    // ymp_minitoptable.style =
    //   "border-top: 1px solid #f1f1f1; border-bottom: 1px solid #f1f1f1; color: #e6e600; table-layout: fixed; width: 100%; word-wrap: break-word;";
    // border light grey and font grey
    ymp_minitoptable.style =
      "border-top: 1px solid #f1f1f1; border-bottom: 1px solid #f1f1f1; color: #b3b3b3; table-layout: fixed; width: 100%; word-wrap: break-word;";

    // header

    var ymp_minitr = ymp_minitoptable.insertRow(-1); // table header row

    var ymp_minith = document.createElement("th"); // table header 1
    ymp_minith.innerHTML = "ID";
    ymp_minith.style = "text-align: left; width: 25%;";
    ymp_minitr.appendChild(ymp_minith);

    var ymp_minith = document.createElement("th"); // table header 2
    ymp_minith.innerHTML = "Job Title";
    ymp_minith.style = "text-align: left; width: 25%;";
    ymp_minitr.appendChild(ymp_minith);

    var ymp_minith = document.createElement("th"); // table header 3
    ymp_minith.innerHTML = "Job Description";
    ymp_minith.style = "text-align: left; width: 50%;";
    ymp_minitr.appendChild(ymp_minith);

    // /header

    // table data

    // create dynamic table
    var ymp_minitable = document.createElement("table"); // table
    ymp_minitable.setAttribute("class", "minitable_group");
    ymp_minitable.style =
      "border-collapse: collapse; table-layout: fixed; width: 100%; word-wrap: break-word;";

    for (var i = 0; i < content_records.length; i++) {

      // get data of the specified group
      if (content_records[i].job_group == group_id) {

        var ymp_minitr = ymp_minitable.insertRow(-1); // table header row

        var ymp_minitd = document.createElement("td"); // table cell 1
        ymp_minitd.innerHTML = content_records[i].job_id;
        // right border light grey
        ymp_minitd.style = "border-right: 1px solid #f1f1f1; text-align: left; width: 25%;";
        ymp_minitr.appendChild(ymp_minitd);

        var ymp_minitd = document.createElement("td"); // table cell 2
        ymp_minitd.innerHTML = content_records[i].job_title;
        // right border light grey
        ymp_minitd.style = "border-right: 1px solid #f1f1f1; text-align: left; width: 25%;";
        ymp_minitr.appendChild(ymp_minitd);

        var ymp_minitd = document.createElement("td"); // table cell 3
        ymp_minitd.innerHTML = content_records[i].job_description;
        ymp_minitd.style = "text-align: left; width: 50%;";
        ymp_minitr.appendChild(ymp_minitd);

      }

    }

    // /table data

    // update Group History

    var ymp_minidiv2 = document.getElementById("job_historyreport2");
    ymp_minidiv2.innerHTML = "";
    ymp_minidiv2.appendChild(ymp_minitable);

    var ymp_minidiv1 = document.getElementById("job_historyreport1");
    ymp_minidiv1.innerHTML = "";
    if (ymp_minitable.offsetHeight > 100) {
      ymp_minitoptable.style =
        "border-top: 1px solid #f1f1f1; border-bottom: 1px solid #f1f1f1; color: #b3b3b3; table-layout: fixed; width: 100%; word-wrap: break-word; padding-right: 14px;";
    }
    ymp_minidiv1.appendChild(ymp_minitoptable);
  }
}
// /group_click

// Photos click action
function photos_click() {

  // get all the related data from JSON Files re Job
  var JSONfile = getJSONfile(
    "tys_production/ympdb/field/field_job.json",
    "tys_production/ympdb/job.json");
  content_fields = JSONfile[0];
  content_fields_head = JSONfile[1];
  content_records = JSONfile[2];
  content_records_head = JSONfile[3];

  // build Pop-Up content

  var minion_h3 = document.createElement("h3"); // header3
  minion_h3.innerHTML = "Job Technical Photos";

  var minion_table = document.createElement("table"); // table
  // set table font color white
  minion_table.setAttribute("style", "color: #fff;");
  minion_table.setAttribute("id", "table_photos");

  JSONfile = top.getJSONfile("", "tys_production/ympdb/popup/job_photos.json");
  var photos_content = JSONfile[2];

  // if the model view is on add and no files have been chosen yet then
  if ((pg_key3 == "add") && (photos_content[0] == "[]")) {

    // increment count
    count_x = count_x + 1;

    var minion_tr = minion_table.insertRow(-1); // row

    var minion_td = minion_tr.insertCell(-1); // cell 1
    minion_td.setAttribute("style", "width: 100px;")
    minion_td.innerHTML = "Photo " + count_x;

    // building block for choosing photo functionality
    var minion_td = minion_tr.insertCell(-1); // cell 2

    var minion_celldiv = document.createElement("div"); // cell divider
    minion_celldiv.setAttribute("id", "div_photo_" + count_x);
    minion_celldiv.setAttribute("style",
      "background-color: #fff; color: #000; height: 63px; margin-right: 10px; width: 128px; overflow:hidden;");

    var minion_celllink = document.createElement("a"); // cell link
    minion_celllink.setAttribute("href", "#");
    minion_celllink.setAttribute("onclick",
      "document.getElementById('input_photo_" + count_x + "').click();");

    var minion_options = document.createElement("img"); // cell image
    minion_options.setAttribute("src", "../../img/ymp_options.png");
    minion_options.setAttribute("style", "height: 15px; width: 15px;");
    minion_options.setAttribute("title", "Select new Photo");
    minion_options.setAttribute("id", "options_photo_" + count_x + "_btn");

    var minion_cellinput = document.createElement("input"); // cell input
    minion_cellinput.setAttribute("type", "file");
    minion_cellinput.setAttribute("id", "input_photo_" + count_x);
    minion_cellinput.setAttribute("class", "file_pic");
    minion_cellinput.addEventListener('change', handleObjectSelect, false);

    var minion_celloutput = document.createElement("output"); // cell output
    minion_celloutput.setAttribute("id", "photo_" + count_x);
    minion_celloutput.innerHTML = "(empty)"

    var minion_img = document.createElement("img"); // cell image
    minion_img.setAttribute("id", "img_photo_" + count_x)

    var minion_remove_link = document.createElement("a");
    minion_remove_link.setAttribute("href", "#");
    minion_remove_link.setAttribute("onclick",
      "removefile('photo_" + count_x + "')"
    );

    var minion_remove_img = document.createElement("img");
    minion_remove_img.setAttribute("id", "remove_photo_" + count_x);
    minion_remove_img.setAttribute("src", "../../img/ymp_removefile.png");
    minion_remove_img.setAttribute("style", "display: none;");
    minion_remove_img.setAttribute("title", "Cancel selected item");

    minion_remove_link.appendChild(minion_remove_img);

    minion_celllink.appendChild(minion_options);
    minion_celldiv.appendChild(minion_celllink);
    minion_celldiv.appendChild(minion_cellinput);
    minion_celldiv.appendChild(minion_celloutput);
    minion_celldiv.appendChild(minion_img);
    minion_celldiv.appendChild(minion_remove_link);

    minion_td.appendChild(minion_celldiv);
    // /building block for choosing photo functionality

    // add new photo button
    var minion_td = minion_tr.insertCell(-1); // cell 3

    var minion_link = document.createElement("a");  // link
    minion_link.setAttribute("href", "#");
    minion_link.setAttribute("onclick", "add_newphoto();");
    minion_link.setAttribute("id", "link_photo_" + count_x);

    var add_photo = document.createElement("img");
    add_photo.setAttribute("src", "../../img/job_addbtn.png");
    add_photo.setAttribute("title", "Add Photo");
    add_photo.setAttribute("id", "img_addphoto_" + count_x);

    minion_link.appendChild(add_photo);
    minion_td.appendChild(minion_link);
    // /add new photo button

  }
  // /add
  else {

    // if no photos have been chosen yet then
    if (photos_content[0] == "[]") {

      // get the related record to edit
      recordtoedit = getObjects(content_records, content_records_head[0], pg_key2);

      photos_content = recordtoedit[0].job_photos;
      refreshJSONrecord("tys_production/ympdb/popup/job_photos.json", photos_content);
      photos_content = JSON.parse("[" + JSON.stringify(photos_content) + "]");

    }

    for (var id_y in photos_content[0]) {

      // increment count
      count_x = count_x + 1;

      var minion_tr = minion_table.insertRow(-1); // row

      var minion_td = minion_tr.insertCell(-1); // cell 1
      minion_td.setAttribute("style", "width: 100px;")
      minion_td.innerHTML = "Photo " + count_x;

      // building block for choosing photo functionality
      var minion_td = minion_tr.insertCell(-1); // cell 2

      var minion_celldiv = document.createElement("div"); // cell divider
      minion_celldiv.setAttribute("id", "div_photo_" + count_x);
      minion_celldiv.setAttribute("style",
        "background-color: #fff; color: #000; height: 63px; margin-right: 10px; width: 128px; overflow:hidden;");

      var minion_celllink = document.createElement("a"); // cell link
      minion_celllink.setAttribute("href", "#");
      minion_celllink.setAttribute("onclick",
        "document.getElementById('input_photo_" + count_x + "').click();");

      var minion_options = document.createElement("img"); // cell image
      minion_options.setAttribute("src", "../../img/ymp_options.png");
      minion_options.setAttribute("style", "height: 15px; width: 15px;");
      minion_options.setAttribute("title", "Select new Photo");
      minion_options.setAttribute("id", "options_photo_" + count_x + "_btn");

      var minion_cellinput = document.createElement("input"); // cell input
      minion_cellinput.setAttribute("type", "file");
      minion_cellinput.setAttribute("id", "input_photo_" + count_x);
      minion_cellinput.setAttribute("class", "file_pic");
      minion_cellinput.addEventListener('change', handleObjectSelect, false);

      var minion_celloutput = document.createElement("output"); // cell output
      minion_celloutput.setAttribute("id", "photo_" + count_x);
      minion_celloutput.innerHTML = ""
      minion_celloutput.alt = photos_content[0][id_y];

      var minion_img = document.createElement("img"); // cell image
      minion_img.setAttribute("id", "img_photo_" + count_x)
      minion_img.setAttribute("src", autochooseicon(photos_content[0][id_y]));
      minion_img.setAttribute("onclick", "opnfile('" + location.href.slice(8, 11)
        + photos_content[0][id_y] + "');");
      minion_img.setAttribute("style",
        "cursor: pointer; margin-top: 6px; max-height: 50px; max-width: 75px;");

      if (pg_key3 != "view") {

        var minion_remove_link = document.createElement("a");
        minion_remove_link.setAttribute("href", "#");
        minion_remove_link.setAttribute("onclick",
          "removefile('photo_" + count_x + "')"
        );

        var minion_remove_img = document.createElement("img");
        minion_remove_img.setAttribute("id", "remove_photo_" + count_x);
        minion_remove_img.setAttribute("src", "../../img/ymp_removefile.png");
        minion_remove_img.setAttribute("title", "Cancel selected item");

        minion_remove_link.appendChild(minion_remove_img);

      }

      minion_celllink.appendChild(minion_options);
      minion_celldiv.appendChild(minion_celllink);
      minion_celldiv.appendChild(minion_cellinput);
      minion_celldiv.appendChild(minion_celloutput);
      minion_celldiv.appendChild(minion_img);
      if (pg_key3 != "view") {
        minion_celldiv.appendChild(minion_remove_link);
      }

      minion_td.appendChild(minion_celldiv);
      // /building block for choosing photo functionality

    }

    if (pg_key3 != "view") {

      // add new photo button
      var minion_td = minion_tr.insertCell(-1); // cell 3

      var minion_link = document.createElement("a");  // link
      minion_link.setAttribute("href", "#");
      minion_link.setAttribute("onclick", "add_newphoto();");
      minion_link.setAttribute("id", "link_photo_" + count_x);

      var add_photo = document.createElement("img");
      add_photo.setAttribute("src", "../../img/job_addbtn.png");
      add_photo.setAttribute("title", "Add Photo");
      add_photo.setAttribute("id", "img_addphoto_" + count_x);

      minion_link.appendChild(add_photo);
      minion_td.appendChild(minion_link);
      // /add new photo button

    }

  }

  var minion_div = document.getElementById("div_popup");
  minion_div.innerHTML = "";
  minion_div.appendChild(minion_h3);
  minion_div.appendChild(minion_table);

  if (pg_key3 != "view") {

    // add save button
    var btn_save = document.createElement("button");
    var btn_save_text = document.createTextNode("Save");
    btn_save.setAttribute("class", "greenbtnmodal");
    btn_save.setAttribute("onclick", "save_photolist();");
    btn_save.appendChild(btn_save_text);

    minion_div.appendChild(btn_save);
    // /add save button

  }

  // add cancel button
  var btn_cancel = document.createElement("button");
  var btn_cancel_text = document.createTextNode("Cancel");
  btn_cancel.setAttribute("class", "redbtnmodal");
  btn_cancel.setAttribute("onclick", "window.close();");
  btn_cancel.appendChild(btn_cancel_text);

  minion_div.appendChild(btn_cancel);
  // /add cancel button

  minion_div.setAttribute("style", "margin-left: 10px;");

}
// /photos_click

// Add new photo select box
function add_newphoto() {

  var remove_addphoto = document.getElementById("img_addphoto_" + count_x);
  remove_addphoto.parentNode.removeChild(remove_addphoto);

  // increment count
  count_x = count_x + 1;

  var minion_table = document.getElementById("table_photos");

  var minion_tr = minion_table.insertRow(-1); // row

  var minion_td = minion_tr.insertCell(-1); // cell 1
  minion_td.setAttribute("style", "width: 100px;")
  minion_td.innerHTML = "Photo " + count_x;

  // building block for choosing photo functionality
  var minion_td = minion_tr.insertCell(-1); // cell 2

  var minion_celldiv = document.createElement("div"); // cell divider
  minion_celldiv.setAttribute("id", "div_photo_" + count_x);
  minion_celldiv.setAttribute("style",
    "background-color: #fff; color: #000; height: 63px; margin-right: 10px; width: 128px; overflow:hidden;");

  var minion_celllink = document.createElement("a"); // cell link
  minion_celllink.setAttribute("href", "#");
  minion_celllink.setAttribute("onclick",
    "document.getElementById('input_photo_" + count_x + "').click();");

  var minion_options = document.createElement("img"); // cell image
  minion_options.setAttribute("src", "../../img/ymp_options.png");
  minion_options.setAttribute("style", "height: 15px; width: 15px;");
  minion_options.setAttribute("title", "Select new Photo");
  minion_options.setAttribute("id", "options_photo_" + count_x + "_btn");

  var minion_cellinput = document.createElement("input"); // cell input
  minion_cellinput.setAttribute("type", "file");
  minion_cellinput.setAttribute("id", "input_photo_" + count_x);
  minion_cellinput.setAttribute("class", "file_pic");
  minion_cellinput.addEventListener('change', handleObjectSelect, false);

  var minion_celloutput = document.createElement("output"); // cell output
  minion_celloutput.setAttribute("id", "photo_" + count_x);
  minion_celloutput.innerHTML = "(empty)"

  var minion_img = document.createElement("img"); // cell image
  minion_img.setAttribute("id", "img_photo_" + count_x)

  var minion_remove_link = document.createElement("a");
  minion_remove_link.setAttribute("href", "#");
  minion_remove_link.setAttribute("onclick",
    "removefile('photo_" + count_x + "')"
  );

  var minion_remove_img = document.createElement("img");
  minion_remove_img.setAttribute("id", "remove_photo_" + count_x);
  minion_remove_img.setAttribute("src", "../../img/ymp_removefile.png");
  minion_remove_img.setAttribute("style", "display: none;");
  minion_remove_img.setAttribute("title", "Cancel selected item");

  minion_remove_link.appendChild(minion_remove_img);

  minion_celllink.appendChild(minion_options);
  minion_celldiv.appendChild(minion_celllink);
  minion_celldiv.appendChild(minion_cellinput);
  minion_celldiv.appendChild(minion_celloutput);
  minion_celldiv.appendChild(minion_img);
  minion_celldiv.appendChild(minion_remove_link);

  minion_td.appendChild(minion_celldiv);
  // /building block for choosing photo functionality

  // add new photo button
  var minion_td = minion_tr.insertCell(-1); // cell 3

  var minion_link = document.createElement("a");  // link
  minion_link.setAttribute("href", "#");
  minion_link.setAttribute("onclick", "add_newphoto();");
  minion_link.setAttribute("id", "link_photo_" + count_x);

  var add_photo = document.createElement("img");
  add_photo.setAttribute("src", "../../img/job_addbtn.png");
  add_photo.setAttribute("title", "Add Photo");
  add_photo.setAttribute("id", "img_addphoto_" + count_x);

  minion_link.appendChild(add_photo);
  minion_td.appendChild(minion_link);
  // /add new photo button
}
// /add_newphoto

// Saves a list of photos for a job to be entered
function save_photolist() {

  var new_records = "[]";
  var valid_counts = 0;

  for (var i = 1; i < count_x + 1; i++) {

    var url_file = document.getElementById("photo_" + i).alt;
    var url_transfer = ""

    if (document.getElementById("input_photo_" + i).value != "") {

      if ((url_file != "") && (url_file != undefined)) {

        // increment valid_counts
        valid_counts = valid_counts + 1;

        url_transfer = document.getElementById("input_photo_" + i).value;
        url_transfer = url_transfer.substring(url_transfer.indexOf("\\fakepath\\")+10);
        url_transfer = content_fields[16][content_fields_head[9]] + "/" + url_transfer;

        var fs = top.require('fs');

        fs.createReadStream(url_file).pipe(
          fs.createWriteStream(location.href.slice(8, 11) + url_transfer));

        url_transfer.replace("\\", "/");

        if (new_records == "[]") {
          new_records = '"photo1":"' + url_transfer + '"';
        }
        else {
          new_records = new_records + ',"photo' + valid_counts + '":"' +
            url_transfer + '"';
        }
      }

    }
    else {
      if (
        (document.getElementById("photo_" + i).alt != "null")
        &&
        (document.getElementById("photo_" + i).alt != "")
        &&
        (document.getElementById("photo_" + i).alt != undefined)
      ) {

        // increment valid_counts
        valid_counts = valid_counts + 1;

        url_transfer = document.getElementById("photo_" + i).alt;

        if (new_records == "[]") {
          new_records = '"photo1":"' + url_transfer + '"';
        }
        else {
          new_records = new_records + ',"photo' + valid_counts + '":"' +
            url_transfer + '"';
        }
      }
    }

  }

  if (new_records != "[]") {
    new_records = '{' + new_records + '}';
    new_records = JSON.parse(new_records);
  }

  document.getElementById("modal_load").style.display="block";

  refreshJSONrecord("tys_production/ympdb/popup/job_photos.json", new_records);

  setTimeout(
    function(){

      window.close();

    }, 2000);

}
// /save_photolist

// Specifically used to handle the selected photos upon clicking the form
function handlephotosclick() {

  // console.log("click works!");

  // increment click_inventory
  click_photos = click_photos + 1;

  // continue with listener until 2nd click
    // 1st click open pop-up
    // 2nd click away from pop-up and stop listener
  if (click_photos > 1) {

    document.getElementById('modal_form').removeEventListener(
      'click', handlephotosclick);
    document.getElementById('modal_form').removeEventListener(
      'mouseover', handlephotosmouse);

    click_photos = 0;

    try{win.close();}catch(err){}

  }

}
// /handlephotosclick

// Specifically used to handle the selected photos upon mouse over the form
function handlephotosmouse() {

  // console.log("mouse over!");

  var JSONfile = top.getJSONfile("", "tys_production/ympdb/popup/job_photos.json");
  var photos_content = JSONfile[2];

  var count_photos = 0;
  var count_12345 = 0;

  for (var id_y in photos_content[0]) {
    if (photos_content[0] != "[]") {
      // increment count
      count_photos = count_photos + 1;
    }
  }

  var ymp_img1 = document.getElementById("img_job_technical_photo1");
  ymp_img1.setAttribute("style", "display: none;");
  var ymp_output1 = document.getElementById("job_technical_photo1");
  ymp_output1.innerHTML = "(empty)";
  var ymp_img2 = document.getElementById("img_job_technical_photo2");
  ymp_img2.setAttribute("style", "display: none;");
  var ymp_output2 = document.getElementById("job_technical_photo2");
  ymp_output2.innerHTML = "(empty)";
  var ymp_img3 = document.getElementById("img_job_technical_photo3");
  ymp_img3.setAttribute("style", "display: none;");
  var ymp_output3 = document.getElementById("job_technical_photo3");
  ymp_output3.innerHTML = "(empty)";
  var ymp_img4 = document.getElementById("img_job_technical_photo4");
  ymp_img4.setAttribute("style", "display: none;");
  var ymp_output4 = document.getElementById("job_technical_photo4");
  ymp_output4.innerHTML = "(empty)";
  var ymp_img5 = document.getElementById("img_job_technical_photo5");
  ymp_img5.setAttribute("style", "display: none;");
  var ymp_output5 = document.getElementById("job_technical_photo5");
  ymp_output5.innerHTML = "(empty)";

  // for the total number of photos from last to first
  for (var i = count_photos; i > 0; i--) {

    // increment count
    count_12345 = count_12345 + 1;

    // case for the last 5 photos selected
    switch (count_12345) {
      case 1:
        ymp_output1.innerHTML = "";
        ymp_img1.setAttribute("src", autochooseicon(photos_content[0]["photo" + i]));
        ymp_img1.setAttribute("style",
          "cursor: pointer; display: inline; margin-top: 6px; max-height: 50px; max-width: 75px;");
        break;
      case 2:
        ymp_output2.innerHTML = "";
        ymp_img2.setAttribute("src", autochooseicon(photos_content[0]["photo" + i]));
        ymp_img2.setAttribute("style",
          "cursor: pointer; display: inline; margin-top: 6px; max-height: 50px; max-width: 75px;");
        break;
      case 3:
        ymp_output3.innerHTML = "";
        ymp_img3.setAttribute("src", autochooseicon(photos_content[0]["photo" + i]));
        ymp_img3.setAttribute("style",
          "cursor: pointer; display: inline; margin-top: 6px; max-height: 50px; max-width: 75px;");
        break;
      case 4:
        ymp_output4.innerHTML = "";
        ymp_img4.setAttribute("src", autochooseicon(photos_content[0]["photo" + i]));
        ymp_img4.setAttribute("style",
          "cursor: pointer; display: inline; margin-top: 6px; max-height: 50px; max-width: 75px;");
        break;
      case 5:
        ymp_output5.innerHTML = "";
        ymp_img5.setAttribute("src", autochooseicon(photos_content[0]["photo" + i]));
        ymp_img5.setAttribute("style",
          "cursor: pointer; display: inline; margin-top: 6px; max-height: 50px; max-width: 75px;");
        break;
    }

  }

}
// /handlephotosmouse
